import { Component, OnInit } from '@angular/core';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { Router } from '@angular/router';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-view-client-details',
  templateUrl: './view-client-details.component.html',
  styleUrls: ['./view-client-details.component.scss']
})
export class ViewClientDetailsComponent implements OnInit {
  viewClientDetails: any =[];
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  phoneNumber =  this.userD.user.phoneNumber;
phoneDisp = "("+ this.phoneNumber +")";
  role = this.userD.user.roleMaster.name;
  constructor(private fb: FormBuilder,private storageService: LotteryStorageService, private route: Router, private alertService: SweetAlertService,
     private lotteryService: LotteryHttpService) {
  }

  ngOnInit() {
      this.viewClientDetails = this.storageService.get('viewUser');
    console.log('hello done',this.viewClientDetails);
  }
 
  initForm() {
    if (this.viewClientDetails.user) {
      return this.fb.group({
        firstName: [this.viewClientDetails.user.firstName],
        lastName: [this.viewClientDetails.user.lastName],
        homeAddress: this.initHomeAddress(),
        currentAddress: this.initCurrentAddress(),
        bandDetail: this.initBankDetails(),
        kycDetail: this.initKycDetails(),
        phoneNumber: [this.viewClientDetails.user.phoneNumber],
        email: [this.viewClientDetails.user.email],
      });
    }
  }

  initHomeAddress() {
    if (this.viewClientDetails.permanentDetail !== null) {
      return this.fb.group({
        shopName: ['', [Validators.required]],
        address: [this.viewClientDetails.permanentDetail.address],
        city: [this.viewClientDetails.permanentDetail.city],
        state: [this.viewClientDetails.permanentDetail.state],
        pin: [this.viewClientDetails.permanentDetail.pin],
        addressType: [this.viewClientDetails.permanentDetail.addressType],
        status: [0],
      });
    } 
  }
  initCurrentAddress() {
    if (this.viewClientDetails.permanentDetail !== null) {
      return this.fb.group({
        shopName: ['', [Validators.required]],
        address: [this.viewClientDetails.permanentDetail.address],
        city: [this.viewClientDetails.permanentDetail.city],
        state: [this.viewClientDetails.permanentDetail.state],
        pin: [this.viewClientDetails.permanentDetail.pin],
        addressType: [this.viewClientDetails.permanentDetail.addressType],
        status: [0],
      });
    } 
  }
  initBankDetails() {
    if (this.viewClientDetails.bankDetail !== null) {
      return this.fb.group({
        bankName: [this.viewClientDetails.bankDetail.docName],
        accountNumber: [this.viewClientDetails.bankDetail.accountNumber],
        ifscCode: [this.viewClientDetails.bankDetail.ifscCode],
        branchName: [this.viewClientDetails.bankDetail.branchName],
      });
    } 
  }
  initKycDetails() {
    if (this.viewClientDetails.kycDetail !== null) {
      return this.fb.group({
        docName: [this.viewClientDetails.kycDetail.docName],
        docNumber: [this.viewClientDetails.kycDetail.docNumber],
      });
    }

  }
}
