import { Component, OnInit } from '@angular/core';
import { LotteryStorageService } from '../../services/lottery-storage.service';
import { LotteryHttpService } from '../../services/lottery-http.service';
import { createCipher } from 'crypto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  loggedInData = {
    'firstName': 'Ayush',
    'lastName': 'Saxena'
  };
  baseImageUrl = '/assets/images/cris.jpg';
  imageUrl: '/assets/images/cris.jpg';
  userD = this.storageService.get('currentUser');
  firstName = this.userD.user.firstName;
  lastName = this.userD.user.lastName;
  phoneNumber =  this.userD.user.phoneNumber;
phoneDisp = "("+ this.phoneNumber +")";
  role = this.userD.user.roleMaster.name;
  today: number = Date.now();
  constructor( private storageService: LotteryStorageService, private lotteryService: LotteryHttpService, private router:Router) {
  }

  ngOnInit() {
  }
  

  navigateTo(value) {
    if (value == 'a') {
      console.log('Profile',value)
        this.router.navigate(['/distributor/admin-profile']);
    }
    else if (value == 'b') {
      console.log('setting',value)
       this.router.navigate(['/client/setting']);
  }
  else if (value == 'c') {
    console.log('logout',value)
   this.router.navigate(['/login']);
}
    
}
}
