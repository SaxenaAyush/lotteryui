import {Component, OnInit} from '@angular/core';
import {ViewChild} from '@angular/core';
import {ElectronService} from '../../../providers/electron.service';
import {TallyConnectorService} from '../../../providers/tallyconnector.service';
import {timingSafeEqual} from 'crypto';
import {Observable} from 'rxjs';
import {errorHandler} from '@angular/platform-browser/src/browser';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {Http} from '@angular/http';
import {map} from 'rxjs/operators';
import {NgForm, FormBuilder} from '@angular/forms';
import {LotteryHttpService} from '../../../services/lottery-http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {LotteryStorageService} from '../../../services/lottery-storage.service';
import {SweetAlertService} from '../../../common/sharaed/sweetalert2.service';
import {FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import * as $ from '../../../../../node_modules/jquery';
import {element} from '@angular/core/src/render3';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';
import {version} from '../../../../../package.json';
import { DatePipe } from '@angular/common';
// import {SimpleTimer} from 'ng2-simple-timer';
// import { UUID } from 'angular2-uuid';
// import { NgxTimerModule } from 'ngx-timer';
// import { Observable } from 'rxjs/Observable'
// import 'rxjs/add/observable/timer'
// import 'rxjs/add/operator/map'
// import 'rxjs/add/operator/take'
// import { Pipe, PipeTransform } from '@angular/core';

// import { timer } from 'rxjs';
// import { Observable } from 'rxjs/Observable'
// import 'rxjs/add/observable/timer'
// import 'rxjs/add/operator/map'
// import 'rxjs/add/operator/take'
// import { Pipe, PipeTransform } from '@angular/core';


@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {

  public loginForm: FormGroup;
  loading: boolean;
  @ViewChild('modal1') modal1: any;
  @ViewChild('modal2') modal2: any;

  @ViewChild('webview') webview: any;

  apiUrl = "";
  flag = 0;
  profile: any = {
    "userId": 0
  };
  url:any = 'http://env-0661550.mj.milesweb.cloud/wallet/';
  
   balanceInfo:any ={
    "id": 7,
    "userName": "123",
    "email": "1@email.com",
    "userId": null,
    "dateCreated": "2019-11-02",
    "balance": 600
};
  examDuration = 1000 * 180;
  leftSelectedNumber: any;
  searchModel: any
  gridApi: any;
  gridColumnApi: any;
  numbers: number[];
  inputarray: number[];
  today: number = Date.now();
  localStorage: any;
  // userDe:any;
  // userDe=JSON.stringify(this.localStorage.get('currentUser'));
singleprice=  this.storageService.get('ticketPrice');
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  retailerID = this.userD.user.id;
  phoneNumber = this.userD.user.phoneNumber;
  Number = "(" + this.phoneNumber + ")";
randomnumber: any;
  countupTimerService: any;
  appVersion:any =' '+ version;
  constructor(private httpClient: HttpClient, private electronService: ElectronService, private storageService: LotteryStorageService,
              private lotteryHttpService: LotteryHttpService, private fb: FormBuilder,
              private router: Router, private route: ActivatedRoute, private alertService: SweetAlertService, private datePipe:DatePipe,private http:HttpClient) {

    setInterval(() => {
      this.today = Date.now()
    }, 1);
    setInterval(() => {
      this.refresh()
    }, 900000);
    // this.userDetails=this.localStorage.getItem.currentUser;
    // console.log('dsvsdv',this.userDetails);
    // console.log(this.localStorage.getItem.currentUser);
  
  }

 t= 22500000;
  //countdown--------------
countdown(){
  // var deadline = new Date("Oct 30, 2019 15:37:25").getTime(); 
  // console.log('deadline',deadline);
  // var x = setInterval(function() { 
  // var now = new Date().getTime(); 
  // console.log('now',now);
  //var t = deadline - now; 
 // var t= 22500000-1000;
  console.log('t---------', this.t);
  var days = Math.floor(this.t / (1000 * 60 * 60 * 24)); 
  var hours = Math.floor((this.t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
  var minutes = Math.floor((this.t % (1000 * 60 * 60)) / (1000 * 60)); 
  console.log('mints', minutes);

  var seconds = Math.floor((this.t % (1000 * 60)) / 1000); 
  console.log('second', seconds);
  document.getElementById("demo").innerHTML =  minutes + "m " + seconds + "s "; 
      if (this.t < 0) { 
         // clearInterval(x); 
          document.getElementById("demo").innerHTML = "EXPIRED";
          this.refresh(); 
      } 
    this.t= this.t-1000;
  // }, 1000); 
}
  
  //----------------------

//   constructor(private storageService: LotteryStorageService,
//     private lotteryHttpService: LotteryHttpService, private fb: FormBuilder,
//     private router: Router, private route: ActivatedRoute, private alertService: SweetAlertService) {
// }

  modelFirst: any = []
  singleModel: any = []
  siglModel: any = {
    "id": 0,
    "value": 0,
    "price": 1,
  }
  modelSecond: any = []

  changeModel: any = {
    "change": 0,
  }


  bookingslot = [
  ]

  buttonsList = [
    {
      "typeid": 1,
      "type": "even",
      "numbersSelected": [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 96, 98],
      "status": "inactive",
      "totalticketAmt": 7,
      "ticketNo": 5
    },
    {
      "typeid": 2,
      "type": "odd",
      "numbersSelected": [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71, 73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99],
      "status": "inactive",
      "totalticketAmt": 7,
      "ticketNo": 7
    },
    {
      "typeid": 3,
      "type": "family",
      "numbersSelected": [0, 2, 4, 8, 10, 12, 98, 78],
      "status": "inactive",
      "ticketNo": 0,
      "totalticketAmt": 7,
      "familyNumber": ''
    },
    {
      "typeid": 4,
      "type": "right",
      "numbersSelected": [9, 18, 27, 36, 45, 54, 63, 72, 81, 90],
      "status": "inactive",
      "totalticketAmt": 7,
      "ticketNo": ''
    },
    {
      "typeid": 5,
      "type": "left",
      "numbersSelected": [0, 11, 22, 33, 44, 55, 66, 77, 88, 99],
      "status": "inactive",
      "ticketNo": '',
      "totalticketAmt": 7,

    },
    {
      "typeid": 6,
      "type": "single",
      "numbersSelected": [24],
      "status": "inactive",
      "ticketNo": '',
      "totalticketAmt": 7,

    },
    {
      "typeid": 9,
      "type": "all",
      "numbersSelected": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,  44,  45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90,  91, 92, 93, 94, 95, 96, 97, 98, 99],
      "status": "inactive",
      "totalticketAmt": 7,
      "ticketNo": 5
    },
  ]
  addslot: any = {
    "bookingDetail": {
      "distributorId": 2,
      "slotId": 0,
      "retailerId": 3,
      "totalPrice": 124
    },
    "tickets": []
  }


  billModel: any = [
    // {
    //   "gherno": 7,
    //   "values": [
    //     {
    //       "typeid": 6,
    //       "type": "single",
    //       "ticketNo": 4,
    //       "totalticketAmt": 7,
    //       "numbersSelected": [26]
    //     },
    //     {
    //       "typeid": 1,
    //       "type": "even",
    //       "ticketNo": 4,
    //       "totalticketAmt": 7,
    //       "numbersSelected": [0, 2, 4, 6, 10]
    //     },
    //     {
    //       "typeid": 2,
    //       "type": "odd",
    //       "ticketNo": 4,
    //       "totalticketAmt": 7,
    //       "numbersSelected": [1, 3, 5, 7, 9]
    //     },
    //     {
    //       "typeid": 3,
    //       "type": "right",
    //       "ticketNo": 5,
    //       "totalticketAmt": 7,
    //       "numbersSelected": [9, 18, 27, 36, 90]
    //     },
    //     {
    //       "typeid": 4,
    //       "type": "left",
    //       "ticketNo": 7,
    //       "totalticketAmt": 7,
    //       "numbersSelected": [0, 11, 22, 33, 99]
    //     },
    //     {
    //       "typeid": 5,
    //       "type": "family",
    //       "ticketNo": 2,
    //       "totalticketAmt": 7,
    //       "numbersSelected": [0, 2, 4, 6, 10],
    //       "roomno": -1,
    //     },

    //   ]
    // }
  ];
  tempModel = {
    "typeid": 0,
    "type": "",
    "ticketNo": 0,
    "totalticketAmt": 0,
    "numbersSelected": [],
    "roomno": -1,
    "no": -1,
    "time": ""
  }

  refreshTempModel() {
    this.tempModel = {
      "typeid": 0,
      "type": "",
      "ticketNo": 0,
      "totalticketAmt": 0,
      "roomno": -1,
      "numbersSelected": [],
      "no": -1,
      "time": ""

    }
  }

  buttonModel = []


  selectedNumber(number: { value: any; }) {
    // if (number.value < 0) {
    //   alert('select your gher');
    // }
    // this.getluckyNumber(number.value);
    this.evenModel.value = number.value;
    this.refreshButtonListAndValues(number);
    this.evenModel.value = number;
    this.selectedNumber1(number.value);
    for (let element of this.modelFirst) {
      for (let element1 of element) {
        element1.status = 'inactive';
        for (let element2 of this.billModel) {

          if (element1.value == number.value) {

            element1.status = 'active';
            this.showModel.room = number.value;
            console.log(this.showModel.room);
          }
          else if (element2.gherno == element1.value) {
            element1.status = 'gray';
          }
        }
      }
    }
  }

  secondModelValues = [];
  secondModelFirstValues = [];

  selectedNumber1(number: { value: any; }) {
    this.familyModel.value = number.value;
    for (let element of this.modelSecond) {
      for (let element1 of element) {
        element1.status = 'inactive';
        this.secondModelValues[element1.value] = '';
        for (let element2 of this.billModel) {
          if (element2.gherno == number) {
            for (let selectedValues of element2.values) {
              this.buttonModel[selectedValues.typeid] = selectedValues.ticketNo;
              this.buttonModel[selectedValues.roomno] = selectedValues.roomno;
              // this.modelSecond[selectedValues.roomno].status='yellow';
              if (element1.value == selectedValues.roomno) {
                element1.status = 'dark';
              }
              for (let valuesraw of selectedValues.numbersSelected) {
                if (element1.value == valuesraw) {
                  if (selectedValues.type == "even") {
                    element1.status = 'active';
                  }
                  else if (selectedValues.type == "odd") {
                    element1.status = 'change';
                  }
                  else if (selectedValues.type == "family") {
                    element1.status = 'yellow';

                  }
                  else if (selectedValues.type == "right") {
                    element1.status = 'danger';
                  }
                  else if (selectedValues.type == "left") {
                    element1.status = 'dark';
                  }
                  else {
                    element1.status = 'active';
                  }

                  if (selectedValues.type == "single") {
                    this.secondModelValues[element1.value] = selectedValues.ticketNo;
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  refreshButtonListAndValues(number: { value: any; }) {

    for (let button in this.buttonModel) {
      this.buttonModel[button] = 0;
      for (let element2 of this.billModel) {
        if (element2.gherno == number) {
          for (let selectedValues of element2.values) {
            if (selectedValues.typeid == selectedValues.typeid)
              this.buttonModel[selectedValues.typeid] = selectedValues.ticketNo;
          }
        }
      }
    }
  }

  eventicketNumber(event: any, _model_id: number) {
    this.gettotal();
    let added = false;
    let fam = 0;
    if (this.evenModel.value == -1) {
      alert('Select Your Gher');
    } else {
      for (let element of this.billModel) {
        if (element.gherno == this.evenModel.value.value) {
          for (let element1 of element.values) {
            if (element1.typeid == event) {
              if (element1.type == 'single') {
              } else if (element1.type == 'family') {
                if (element1.roomno != this.familyModel.value) {

                  alert('you can not select another family on same gher');
                  fam = 1;
                }

                // element1.ticketNo = this.buttonModel[event];
                // added = true;
                // this.selectedNumber(this.evenModel.value);
              } else {
                element1.ticketNo = this.buttonModel[event];
                element1.totalticketAmt= this.getPriceModel.familyPrice * element1.ticketNo;
                added = true;
                this.selectedNumber(this.evenModel.value);
              }
            }
          }
          if (added == false) {
            this.refreshTempModel();
            for (let button of this.buttonsList) {
              if (button.typeid == event) {
                if (button.type == 'single') {

                } else {
                  if (button.type != 'family') {


                    button.ticketNo = this.buttonModel[event];

                    button['totalticketAmt'] = this.getPriceModel.familyPrice * this.buttonModel[event];
                    
                    element.values.push(
                      button
                    )
                    added = true;
                    console.log(this.billModel);
                    this.selectedNumber(this.evenModel.value);
                  }
                }
              }
            }
          }
        }
      }
      if (added == false) {
        this.refreshTempModel();
        for (let button of this.buttonsList) {
          if (button.typeid == event) {
            if (button.type == 'single') {
            }
            else if (button.type == 'family') {

              if (this.ctrlModel.number == true) {

                for (let ctr of this.gherarray) {
                  if (ctr.status == 1) {
                    this.familyModel.value = this.familyModel.rooms;
                  //  this.FamilysingleNumber();
                    let Fflag = false;
                    for (let check of this.billModel) {
                      if (check.gherno == this.evenModel.value.value) {
                        if (check.roomno != this.familyModel.value) {
                          alert('you can not select another family on same gher');
                          Fflag = true;
                          fam=1;
                        }
                      }
                    }
                    if (fam == 0) {

                      for(let ar of this.array){
                        if(this.familyModel.value==ar.value)
                        {
                         this.tempModel.numbersSelected = ar.numbersSelected;
                        }
                      
                      }        
                    this.tempModel.typeid = 3;
                  
                    this.tempModel.ticketNo = this.buttonModel[event];
                    this.tempModel.totalticketAmt = this.getPriceModel.familyPrice *  this.tempModel.ticketNo;
                 
                    this.billModel.push({
                      "gherno": ctr.gherno,
                      "values": [this.tempModel]
                    })
                    console.log(this.billModel);
               
                   this.refreshTempModel();
                    }

                  }


                }

                this.refreshTempModel();
                this.gherarray = [];
                this.ctrlModel.number = false;
              }
              else {
                let Fflag = false;
                for (let check of this.billModel) {
                  if (check.gherno == this.evenModel.value.value) {
                    if (check.roomno != this.familyModel.value) {
                      alert('you can not select another family on same gher');
                      Fflag = true;
                    }
                  }
                }
                if (fam == 0) {
                  if (this.familyModel.value == -1) {
                    alert('select your number first');
                  } else {

                       for(let ar of this.array){
                         if(this.familyModel.value==ar.value)
                         {
                          this.tempModel.numbersSelected = ar.numbersSelected;
                         }
                       
                       }        
                     this.tempModel.typeid = 3;
                   
                     this.tempModel.ticketNo = this.buttonModel[event];
                     this.tempModel.totalticketAmt = this.getPriceModel.familyPrice *  this.tempModel.ticketNo;
                  
                     this.billModel.push({
                       "gherno": this.evenModel.value.value,
                       "values": [this.tempModel]
                     })
                     console.log(this.billModel);
                
                    this.refreshTempModel();
                  
                 
                   }


                }
              }


            }
            // if(button.type !='single'){
            else {

              if (this.ctrlModel.number == true) {
                for (let ctr of this.gherarray) {
                  if (ctr.status == 1) {
                    if (button.type == 'even')
                    {
                      button.ticketNo = +this.buttonModel[event];
                      button['totalticketAmt'] = this.getPriceModel.evenPrice * this.buttonModel[event];
                      this.billModel.push({
                        "gherno": ctr.gherno,
                        "values": [button],
  
                      });
                    }else if(button.type == 'odd'){
                      button.ticketNo = +this.buttonModel[event];
                      button['totalticketAmt'] = this.getPriceModel.oddPrice * this.buttonModel[event];
                      this.billModel.push({
                        "gherno": ctr.gherno,
                        "values": [button],
  
                      });
                    }else if(button.type == 'left'){
                      button.ticketNo = +this.buttonModel[event];
                      button['totalticketAmt'] = this.getPriceModel.leftPrice * this.buttonModel[event];
                      this.billModel.push({
                        "gherno": ctr.gherno,
                        "values": [button],
  
                      });
                    }else if(button.type == 'right'){
                      button.ticketNo = +this.buttonModel[event];
                      button['totalticketAmt'] = this.getPriceModel.rightPrice * this.buttonModel[event];
                      this.billModel.push({
                        "gherno": ctr.gherno,
                        "values": [button],
  
                      });
                    }
                    else if(button.type == 'all'){
                      button.ticketNo = +this.buttonModel[event];
                      button['totalticketAmt'] = this.getPriceModel.allPrice * this.buttonModel[event];
                      this.billModel.push({
                        "gherno": ctr.gherno,
                        "values": [button],
  
                      });
                    }
                  }
                }
                this.refreshTempModel();
                this.gherarray = [];
                this.ctrlModel.number = false;
                console.log(this.billModel);
                this.selectedNumber(this.evenModel.value);
              }
              else {
                 if(button.type == 'right'){
                  button.ticketNo = +this.buttonModel[event];
                  button['totalticketAmt'] = this.getPriceModel.rightPrice * this.buttonModel[event];
                  this.billModel.push({
                    "gherno": +this.evenModel.value.value,
                    "values": [button],
  
                  });
                  console.log(this.billModel);
                  this.selectedNumber(this.evenModel.value);
                }else if(button.type == 'left'){
                  button.ticketNo = +this.buttonModel[event];
                  button['totalticketAmt'] = this.getPriceModel.leftPrice * this.buttonModel[event];
                  this.billModel.push({
                    "gherno": +this.evenModel.value.value,
                    "values": [button],
  
                  });
                  console.log(this.billModel);
                  this.selectedNumber(this.evenModel.value);
                }else if(button.type == 'even'){
                  button.ticketNo = +this.buttonModel[event];
                  button['totalticketAmt'] = this.getPriceModel.evenPrice * this.buttonModel[event];
                  this.billModel.push({
                    "gherno": +this.evenModel.value.value,
                    "values": [button],
  
                  });
                  console.log(this.billModel);
                  this.selectedNumber(this.evenModel.value);
                }else if(button.type == 'odd'){
                  button.ticketNo = +this.buttonModel[event];
                  button['totalticketAmt'] = this.getPriceModel.oddPrice  * this.buttonModel[event];
                  this.billModel.push({
                    "gherno": +this.evenModel.value.value,
                    "values": [button],
  
                  });
                  console.log(this.billModel);
                  this.selectedNumber(this.evenModel.value);
                }
                else if(button.type == 'all'){
                  button.ticketNo = +this.buttonModel[event];
                  button['totalticketAmt'] = this.getPriceModel.allPrice  * this.buttonModel[event];
                  this.billModel.push({
                    "gherno": +this.evenModel.value.value,
                    "values": [button],
  
                  });
                  console.log(this.billModel);
                  this.selectedNumber(this.evenModel.value);
                }

                
              }

            }
          }
        }

      }
    }
    this.gettotal();

  }

  FNumber(_number: any) {
    this.familyModel.value = _number;
    this.familyModel.rooms = _number;

  }

  singleSelection(input: any) {
    this.gettotal();
    let add = false;
    this.refreshTempModel();

    for (let element of this.billModel) {
      if (element.gherno == this.evenModel.value.value) {

        for (let element1 of element.values) {

          if (element1.type == 'single') {
            if (input == element1.numbersSelected[0]) {
              element1.ticketNo = this.secondModelValues[input];
              element1.totalticketAmt= this.getPriceModel.singlePrice * element1.ticketNo;
              add = true;
            }
          }

        }
        if (add == false) {
          this.tempModel.typeid = 6;
          this.tempModel.numbersSelected = [input];
          this.tempModel.ticketNo = this.secondModelValues[input];
          this.tempModel.type = 'single';
          this.tempModel.totalticketAmt = this.getPriceModel.singlePrice * this.tempModel.ticketNo;
          element.values.push(
            this.tempModel
          )
          this.refreshTempModel();
          add = true;
        }
      }
    }
    if (add == false) {
      if (this.ctrlModel.number == true) {
        for (let ctr of this.gherarray) {
          if (ctr.status == 1) {
            this.tempModel.typeid = 6;
            this.tempModel.numbersSelected = [input];
            this.tempModel.ticketNo = this.secondModelValues[input];
            this.tempModel.type = 'single';
            this.tempModel.totalticketAmt = this.getPriceModel.singlePrice * this.tempModel.ticketNo;
            this.billModel.push({
              "gherno": ctr.gherno,
              "values": [this.tempModel]
            })
          }

        }

        this.refreshTempModel();
        this.gherarray = [];
        add = true;
        this.ctrlModel.number = false;
      }
      else {
        this.tempModel.typeid = 6;
        this.tempModel.numbersSelected = [input];
        this.tempModel.ticketNo = this.secondModelValues[input];
        this.tempModel.type = 'single';
        this.tempModel.totalticketAmt = this.getPriceModel.singlePrice * this.tempModel.ticketNo;
        this.billModel.push({
          "gherno": this.evenModel.value.value,
          "values": [this.tempModel]
        })
        this.refreshTempModel();
        add = true;
      }

    }

    this.gettotal();
  }

  // first(input: any){
  //   let add=false;
  //   this.refreshTempModel();

  //   for (let element of this.billModel) {
  //     if (element.gherno == this.evenModel.value.value) {

  //       for (let element1 of element.values) {

  //           if(element1.type == 'front' ){
  //            if(input==element1.numbersSelected[0]){
  //                 element1.ticketNo=this.secondModelFirstValues[input];
  //                 add=true;
  //            }
  //           }

  //       }
  //       if(add==false){
  //        this.tempModel.typeid=7;
  //       //  this.tempModel.numbersSelected=[input];
  //        this.tempModel.ticketNo=this.secondModelFirstValues[input];
  //        this.tempModel.type='front';
  //        this.tempModel.totalticketAmt=50;
  //        element.values.push(
  //         this.tempModel
  //        )
  //        this.refreshTempModel();
  //        add=true;
  //       }
  //     }
  //   }
  //     if(add==false){
  //       this.tempModel.typeid=6;
  //       this.tempModel.numbersSelected=[input];
  //       this.tempModel.ticketNo=this.secondModelFirstValues[input];
  //       this.tempModel.type='front';
  //       this.tempModel.totalticketAmt=50;
  //       this.billModel.push({
  //         "gherno" : this.evenModel.value.value,
  //        "values" : [this.tempModel]
  //       })
  //       this.refreshTempModel();
  //       add=true;
  //      }


  // }
  evenModel: any = {
    "value": -1,
    "ticket": 0,
    "ticketno": 0,
    "oddticketno": 0,
    "leftticketno": 0,
    "rightticketno": 0,
    "backPrice": 10,
    "ticketPrice": 50,
    "familyticketPrice": 8,
    "leftticketPrice": 10,
    "totalticketAmt": 0,
    "type": "",
    "backno": -1,
    "fno": "",
    "roomno": "",

  }
  multigherstore: any = [];
  multigherclonestore: any = [];
  multModel: any = {
    "gherno": -1,
    "showgherno": "",
    "flag": 0,
  }
  gherModel: any = {
    "gher": 0,
  }

  frontNumber: any = []
  timeTable: any = []
  frontModel: any = {
    "id": "",
  }

  numberModel: any = {
    "value": 0,
  }
  showModel: any = {
    "value": 0,
    "temp": "",
    "temp1": "",
    "totalAmount": 0,
    "room": 0,
  }

  familyModel: any = {
    "single": 0,
    "value": -1,
    "temp1": "",
    "totalAmount": 0,
    "room": 0,
    "ticketPrice": 8,
    "setvar": "",
    "first": "",
    "back": "",
    "ticketno": 0,
    "rooms": -1,
  }

  backModel: any = {
    "backno": -1,
    "ticket": 0,
    "temp1": "",
    "totalAmount": 0,
    "room": 0,
    "ticketPrice": 8,
    "setvar": "",
    "ticketno": 0,
  }
  firstModel: any = {
    "firstno": -1,
    "ticket": 0,
    "temp1": "",
    "totalAmount": 0,
    "room": 0,
    "ticketPrice": 8,
    "setvar": "",
    "ticketno": 0,
  }

  checkboxModel: any = {
    "value": '',
  }

  totalModel: any = {
    "total": 0,
  }
  len: { [s: string]: unknown; } | ArrayLike<unknown>;

  // billModel: any = {
  //    "id" : 0,
  //    "gher": "",
  //    "ticketno" : 0,
  //    "totalticketAmt" : this.evenModel.ticketno,
  // }

  IsVisible = true;


  familyArray: any = [];

  familyResultArray: any = [];


  model: any;

  // ---------------------------- Front Operation ------------------------

  firstNumber(_event: any) {
    console.log('first number', _event);
    this.firstModel.firstno = _event;
    if (this.evenModel.value != -1) {
    } else {
      alert('Select Your Gher');
    }
  }

  first(_event: any) {
    this.gettotal();
    let flag = 0;
    this.firstModel.ticket = _event.target.value;
    if (this.evenModel.value != -1) {
      for (let element of this.billModel) {
        if (this.evenModel.value.value == element.gherno) {

          for (let element1 of element.values) {
            for (let element2 of element.values) {
              if (element1.type == 'front') {
                if (element1.no == this.firstModel.firstno) {
                  element2.ticketNo = this.firstModel.ticket;
                  element2.totalticketAmt= this.getPriceModel.frontPrice * element2.ticketNo;

                  flag = 1;
                }
              }
            }

          }
        }


      }
      if (flag != 1) {
        if (this.ctrlModel.number == true) {
          for (let ctr of this.gherarray) {
            if (ctr.status == 1) {
              this.tempModel.ticketNo = _event.target.value;
              this.tempModel.type = 'front';
              this.tempModel.typeid = 7;
              this.tempModel.totalticketAmt = this.getPriceModel.frontPrice *  this.tempModel.ticketNo;
              this.tempModel.no = this.firstModel.firstno;
              this.firstroom(this.firstModel.firstno);
              this.shownoF();
              var b = this.familyModel.first.split(',');
              console.log('b', b);
              var c = b.map(Number);
              console.log('c', c);
              this.tempModel.numbersSelected = c;
              console.log(_event.target.value);
              // this.addLotteryNo();
              this.billModel.push({
                "gherno": +ctr.gherno,
                "values": [this.tempModel]
              });
              console.log('billmodel', this.billModel);
              this.familyModel.first = "";
              this.firstModelno = [];
              // this.refreshTempModel();
            }

          }
          this.refreshTempModel();
          this.gherarray = [];
          this.ctrlModel.number = false;
          console.log(this.billModel);
          this.selectedNumber(this.evenModel.value);
        } else {
          this.tempModel.ticketNo = _event.target.value;
          this.tempModel.type = 'front';
          this.tempModel.typeid = 7;
          this.tempModel.totalticketAmt =this.getPriceModel.frontPrice *  this.tempModel.ticketNo;
          this.tempModel.no = this.firstModel.firstno;
          this.firstroom(this.firstModel.firstno);
          this.shownoF();
          var b = this.familyModel.first.split(',');
          console.log('b', b);
          var c = b.map(Number);
          console.log('c', c);
          this.tempModel.numbersSelected = c;
          console.log(_event.target.value);
          // this.addLotteryNo();
          this.billModel.push({
            "gherno": +this.evenModel.value.value,
            "values": [this.tempModel]
          });
          console.log('billmodel', this.billModel);
          this.familyModel.first = "";
          this.firstModelno = [];
          this.refreshTempModel();
        }


      }

    }
    else {
      alert('Select your Gher');
    }
    this.gettotal();
  }

  firstroom(_value: string | number) {
    this.gherModel.gher = _value;
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[_value][j].value == this.modelSecond[i][j].value) {
          this.modelSecond[i][j].status = 'yellow';
          this.firstModelno.push({
            "value": this.modelSecond[i][j].value,
          });
        }

      }
    }
  }

  shownoF() {
    console.log('firstModelno', this.firstModelno);
    for (let i = 0; i < this.firstModelno.length; i++) {

      if (this.firstModelno[this.firstModelno.length - 1].value == this.firstModelno[i].value) {
        this.familyModel.first += this.firstModelno[i].value;
      } else {
        this.familyModel.first += this.firstModelno[i].value + ",";
      }

    }
    console.log(this.familyModel.first);
  }

  shownoB() {
    console.log('backModelno', this.backModelno);
    for (let i = 0; i < this.backModelno.length; i++) {

      if (this.backModelno[this.backModelno.length - 1].value == this.backModelno[i].value) {
        this.familyModel.back += this.backModelno[i].value;
      } else {
        this.familyModel.back += this.backModelno[i].value + ",";
      }

    }
    console.log(this.familyModel.back);
  }

  firstModelno: any = [];

  inactivefrontroom(_value: string | number) {
    this.gherModel.gher = _value;
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[_value][j].value == this.modelSecond[i][j].value) {
          this.modelSecond[i][j].status = 'inactive';
        }

      }
    }
  }

  //-------------------------------End Front Operation---------------------------------------

  //-----------------------------Back Operation --------------------------

  backNumber(_event: any) {
    let temp: any;
    temp = _event;
    this.backModel.backno = temp;
    console.log(this.backModel.backno);
    if (this.evenModel.value != -1) {
    } else {
      alert('Select Your Gher');
    }
  }

  back(_event: any) {
    let flag = 0;
    this.backModel.ticket = _event.target.value;
    if (this.evenModel.value != -1) {
      for (let element of this.billModel) {
        if (this.evenModel.value.value == element.gherno) {

          for (let element1 of element.values) {
            for (let element2 of element.values) {
              if (element1.type == 'back') {
                if (element1.no == this.backModel.backno) {
                  element2.ticketNo = this.backModel.ticket;
                  element2.totalticketAmt= this.getPriceModel.backPrice * element2.ticketNo;
                  flag = 1;
                }
              }
            }

          }
        }


      }
      if (flag != 1) {
        if (this.ctrlModel.number == true) {
          for (let ctr of this.gherarray) {
            if (ctr.status == 1) {
              this.tempModel.ticketNo = _event.target.value;
              this.tempModel.type = 'back';
              this.tempModel.typeid = 8;
              this.tempModel.totalticketAmt = this.getPriceModel.backPrice *   this.tempModel.ticketNo;
              this.tempModel.no = this.backModel.backno;

              this.backroom(this.backModel.backno);
              this.shownoB();
              var b = this.familyModel.back.split(',');
              console.log('b', b);
              var c = b.map(Number);
              console.log('c', c);
              this.tempModel.numbersSelected = c;
              console.log(_event.target.value);
              // this.addLotteryNo();
              this.billModel.push({
                "gherno": ctr.gherno,
                "values": [this.tempModel]
              });


              this.familyModel.back = "";
              this.backModelno = [];
            }

            // this.refreshTempModel();
          }
          this.refreshTempModel();
          this.gherarray = [];
          this.ctrlModel.number = false;
          console.log(this.billModel);
          this.selectedNumber(this.evenModel.value);
        }
        else {
          this.tempModel.ticketNo = _event.target.value;
          this.tempModel.type = 'back';
          this.tempModel.typeid = 8;
          this.tempModel.totalticketAmt = this.getPriceModel.backPrice *   this.tempModel.ticketNo;
          this.tempModel.no = this.backModel.backno;

          this.backroom(this.backModel.backno);
          this.shownoB();
          var b = this.familyModel.back.split(',');
          console.log('b', b);
          var c = b.map(Number);
          console.log('c', c);
          this.tempModel.numbersSelected = c;
          console.log(_event.target.value);
          // this.addLotteryNo();
          this.billModel.push({
            "gherno": +this.evenModel.value.value,
            "values": [this.tempModel]
          });


          this.familyModel.back = "";
          this.backModelno = [];
          this.refreshTempModel();
        }

      }
      this.gettotal();
    }
    else {
      alert('Select your Gher');
    }
    this.gettotal();
  }

  backroom(_value: string | number) {
    this.gherModel.gher = _value;
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[i][_value].value == this.modelSecond[i][j].value) {
          this.modelSecond[i][j].status = 'yellow';
          this.backModelno.push({
            "value": this.modelSecond[i][j].value,
          });
        }

      }
    }
  }

  backModelno: any = [];

  inactivebackroom(_value: string | number) {
    this.gherModel.gher = _value;
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[i][_value].value == this.modelSecond[i][j].value) {
          this.modelSecond[i][j].status = 'inactive';
        }

      }
    }
  }

  //------------ Multi Gher-----------------------
  multighrclone() {
    for (let i = 0; i < this.billModel.length; i++) {
      let flag = 0;
      if (this.multModel.gherno == this.billModel[i].gherno) {
        this.multigherclonestore.push({
          "data": this.billModel[i],
        });
      }
    }
    console.log('clone', this.multigherclonestore);
  }

  addclonedata() {
    this.multModel.flag = 0;
    this.gettotal();
    for (let i = 0; i < this.multigherstore.length; i++) {
      for (let j = 0; j < this.multigherclonestore.length; j++) {
        if (this.multigherclonestore[j].data.type = "even") {
          let temp = "Gher" + "(" + this.multigherstore[i].gher + ")" + "Even";
          this.billModel.push({
            "gherno": this.multigherstore[i].gher,
            "gher": temp,
            "ticketno": this.multigherclonestore[j].data.ticketno,
            "totalticketAmt": this.multigherclonestore[j].data.totalticketAmt,
            "type": this.multigherclonestore[j].data.type,
            "backno": this.multigherclonestore[j].data.backno,
            "fno": this.multigherclonestore[j].data.fno,
            "roomno": this.multigherclonestore[j].data.roomno,
          });
        } else if (this.multigherclonestore[j].data.type = "odd") {
          let temp = "Gher" + "(" + this.multigherstore[i].gher + ")" + "Odd";
          this.billModel.push({
            "gherno": this.multigherstore[i].gher,
            "gher": temp,
            "ticketno": this.multigherclonestore[j].data.ticketno,
            "totalticketAmt": this.multigherclonestore[j].data.totalticketAmt,
            "type": this.multigherclonestore[j].data.type,
            "backno": this.multigherclonestore[j].data.backno,
            "fno": this.multigherclonestore[j].data.fno,
            "roomno": this.multigherclonestore[j].data.roomno,
          });
        } else if (this.multigherclonestore[j].data.type = "front") {
          let temp = "Gher" + "(" + this.multigherstore[i].gher + ")" + "Front" + "(" + this.multigherstore[i].backno + ")";
          this.billModel.push({
            "gherno": this.multigherstore[i].gher,
            "gher": temp,
            "ticketno": this.multigherclonestore[j].data.ticketno,
            "totalticketAmt": this.multigherclonestore[j].data.totalticketAmt,
            "type": this.multigherclonestore[j].data.type,
            "backno": this.multigherclonestore[j].data.backno,
            "fno": this.multigherclonestore[j].data.fno,
            "roomno": this.multigherclonestore[j].data.roomno,
          });
        } else if (this.multigherclonestore[j].data.type = "back") {
          let temp = "Gher" + "(" + this.multigherstore[i].gher + ")" + "Back" + "(" + this.multigherstore[i].backno + ")";
          this.billModel.push({
            "gherno": this.multigherstore[i].gher,
            "gher": temp,
            "ticketno": this.multigherclonestore[j].data.ticketno,
            "totalticketAmt": this.multigherclonestore[j].data.totalticketAmt,
            "type": this.multigherclonestore[j].data.type,
            "backno": this.multigherclonestore[j].data.backno,
            "fno": this.multigherclonestore[j].data.fno,
            "roomno": this.multigherclonestore[j].data.roomno,
          });
        }
        else if (this.multigherclonestore[j].data.type = "family") {
          let temp = "Gher" + "(" + this.multigherstore[i].gher + ")" + "F" + "(" + this.multigherstore[i].roomno + ")" + ":-" + this.multigherstore[i].fno;
          this.billModel.push({
            "gherno": this.multigherstore[i].gher,
            "gher": temp,
            "ticketno": this.multigherclonestore[j].data.ticketno,
            "totalticketAmt": this.multigherclonestore[j].data.totalticketAmt,
            "type": this.multigherclonestore[j].data.type,
            "backno": this.multigherclonestore[j].data.backno,
            "fno": this.multigherclonestore[j].data.fno,
            "roomno": this.multigherclonestore[j].data.roomno,
          });
        } else if (this.multigherclonestore[j].data.type = "single") {
          let temp = "Gher" + "(" + this.multigherstore[i].gher + ")" + "S" + "(" + this.multigherstore[i].roomno + ")";
          this.billModel.push({
            "gherno": this.multigherstore[i].gher,
            "gher": temp,
            "ticketno": this.multigherclonestore[j].data.ticketno,
            "totalticketAmt": this.multigherclonestore[j].data.totalticketAmt,
            "type": this.multigherclonestore[j].data.type,
            "backno": this.multigherclonestore[j].data.backno,
            "fno": this.multigherclonestore[j].data.fno,
            "roomno": this.multigherclonestore[j].data.roomno,
          });
        }

      }
    }
    this.gettotal();

  }


  //---------------------------------------------

  //---------------------------End Back Operation-------------------------

  show() {
    console.log(this.evenModel.value);
  }

  //  public showNotification(type: string, message: string): void {
  //   this.notifier.notify(type, message);
  // }
  // public hideAllNotifications(): void {
  //   this.notifier.hideAll();
  // }


  // selectedNumber(number: { value: any; }){
  //   for(let element of this.modelFirst){
  //     for(let element1 of element){
  //       if (element1.value == number.value){
  //         element1.status = 'active';
  //         this.evenModel.value =number.value;
  //         this.showModel.value =this.evenModel.value;
  //         this.show();
  //         this.inactive();
  //         this.inactiveOdd();
  //         this.showModel.temp = "";
  //         this.evenModel.ticket = "";

  //       }
  //       else{

  //       }
  //     }
  //   }
  //   for(let i=0;i<this.modelFirst.length;i++)
  //     {
  //       for(let j=0;j<this.modelFirst[i].length;j++){

  //         for(let k=0;k<this.billModel.length;k++){
  //           if(this.modelFirst[i][j].value==this.billModel[k].gherno)
  //           {
  //             this.modelFirst[i][j].status="gray";
  //           }
  //         }
  //       }
  //     }
  //     this.createSecondModel();
  //   if(this.multModel.flag==1){
  //     this.multigherstore.push( {
  //       "gher": this.evenModel.value,
  //     });
  //     console.log(this.multigherstore);
  //     for(let i=0;i<this.modelFirst.length;i++)
  //     {
  //       for(let j=0;j<this.modelFirst[i].length;j++){
  //         if(this.evenModel.value==this.modelFirst[i][j].value)
  //         {
  //           this.modelFirst[i][j].status = 'gray';
  //         }
  //       }
  //     }

  //     console.log('multiclobe',this.multigherclonestore);

  //   }

  //   for(let i=0;i<this.modelFirst.length;i++)
  //   {
  //     for(let j=0;j<this.modelFirst[i].length;j++){
  //       if(this.modelFirst[i][j].status=="gray"){

  //       }
  //       else{
  //         if(this.evenModel.value==this.modelFirst[i][j].value)
  //         {
  //           continue;
  //         }
  //         else{
  //           this.modelFirst[i][j].status = 'inactive';
  //         }

  //       }

  //     }
  //   }
  // }


  oddticketNumber(event: any) {
    if (this.evenModel.value != -1) {
      this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Odd";
      this.evenModel.oddticketno = event.target.value;
      this.evenModel.ticketno = event.target.value;
      this.evenModel.totalticketAmt = this.evenModel.ticketPrice * this.evenModel.ticketno;
      this.evenModel.type = "odd";
      this.OddShow();


      this.gherchangeSingleColor(this.evenModel.value);
    }
    else {
      this.evenModel.oddticketno = 0;
      alert('Select Your Gher First');

    }

  }

  leftticketNumber(event: any) {
    if (this.evenModel.value != -1) {
      this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Left";
      this.evenModel.leftticketno = event.target.value;
      this.evenModel.ticketno = this.evenModel.leftticketno;
      this.evenModel.totalticketAmt = this.evenModel.leftticketPrice * this.evenModel.ticketno;
      this.double();
      //this.addLotteryNo();
    }
    else {
      this.evenModel.ticketno = 0;
      this.evenModel.leftticketno = 0;
      alert('Select your gher First');

    }


  }

  rightticketNumber(event: any) {
    if (this.evenModel.value != -1) {
      this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Right";
      this.evenModel.rightticketno = event.target.value;
      this.evenModel.ticketno = this.evenModel.rightticketno;
      this.evenModel.totalticketAmt = this.evenModel.leftticketPrice * this.evenModel.ticketno;
      this.right();
      //this.addLotteryNo();
    }
    else {
      this.evenModel.ticketno = 0;
      this.evenModel.leftticketno = 0;
      alert('Select your gher First');

    }


  }

  totalTicketAmount() {

    this.showModel.totalAmount = this.showModel.totalAmount + this.evenModel.totalticketAmt;
  }

  createFirstModel() {
    let tempArray = [];
    for (var i = 0; i < 100; i++) {
      if (i <= 9) {
        let temp: string;
        tempArray.push({
          "value": i,
          "name": '0'+i,
          "status": "inactive"
        });
      }
      else {
        tempArray.push({
          "value": i,
          "name": i,
          "status": "inactive"
        });
      }
      if ((i + 1) % 10 == 0) {
        this.modelFirst.push(tempArray);
        tempArray = [];
      }
    }

  }

  //------------------Single---------------------

  gherchangeSingleColor(value: any) {
    console.log(value);
    for (let i = 0; i <= this.modelFirst.length - 1; i++) {
      for (let j = 0; j <= this.modelFirst[i].length - 1; j++) {
        if (this.modelSecond[i][j].value == value) {
          console.log(this.modelFirst[i][j].value);
          this.modelFirst[i][j].status = 'gray';

        }
      }
    }
  }

  changeSingleColor(value: any) {
    console.log(value);
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[i][j].value == value) {
          console.log(this.modelSecond[i][j].value);
          this.modelSecond[i][j].status = 'gray';

        }
      }
    }
  }

  SingleNumber(selectedItem: any) {
    this.siglModel.id = selectedItem;
    console.log('this.sigmodel.id', this.siglModel.id);
    console.log("Selected item Id: ", selectedItem);
    //this.billModel.splice(this.billModel.indexOf(selectedItem), 1);
  }

  singleNumberValue(_event: any) {
    let flag = 0;
    this.siglModel.value = _event.target.value;
    for (let i = 0; i < this.billModel.length; i++) {
      let temp = this.showModel.value + this.siglModel.id + " ";
      if (temp == this.billModel[i].gher) {
        this.billModel[i].ticketno = this.siglModel.value;
        this.billModel[i].totalticketAmt = this.siglModel.price * this.siglModel.value;
        flag = 1;
      }
    }
    if (this.showModel.value != 0) {
      if (flag != 1) {
        this.siglModel.value = _event.target.value;
        console.log(this.siglModel.value);
        this.evenModel.ticketno = this.siglModel.value;
        this.evenModel.totalticketAmt = this.siglModel.price * this.evenModel.ticketno;
        this.showModel.temp = this.showModel.value + this.siglModel.id + " ";
        this.evenModel.type = "single";
        this.evenModel.roomno = this.siglModel.id;
        // this.totalTicketAmount();
        this.addLotteryNo();
        this.changeSingleColor(this.siglModel.id);
        this.gherchangeSingleColor(this.showModel.value);

      }

    }
    else {
      alert('select your gher');
    }

  }


  //--------------------End Single--------------------------
  //-------------------front ---------------------
  front() {
    // let tempArray =[];
    for (var i = 0; i < 10; i++) {

      this.frontNumber.push({
        "value": i,
        "name": i,

      });

    }

  }

  time() {
    // let tempArray =[];
    let hr = 10;
    for (var i = 0; i < 11; i++) {
      let k = 0;

      let min = 15;
      for (let j = 0; j < 4; j++) {
        if (j == 3) {

          let time = (hr + 1) + ":" + "00";
          this.timeTable.push({
            "value": time,
            "id": k++,
            "status": 'inactive',
          });
          min = min + 15;
        }
        else {
          let time = hr + ":" + min;
          this.timeTable.push({
            "value": time,
            "id": k++,
            "status": 'inactive',
          });
          min = min + 15;
        }

      }
      hr = hr + 1;
    }

  }

  
 

  //---------------------------------------------

  // Family -------------------************FAMILY*************----------------------------
  showno() {
    console.log('setmodel1', this.setmodel1);
    for (let i = 0; i < this.setmodel1.length; i++) {

      if (this.setmodel1[this.setmodel1.length - 1].value == this.setmodel1[i].value) {
        this.familyModel.setvar += this.setmodel1[i].value;
      } else {
        this.familyModel.setvar += this.setmodel1[i].value + ",";
      }

    }
    console.log(this.familyModel.setvar);
  }

  FamilyticketNumber(_event: any) {

    // this.evenModel.ticketno = this.familyModel.ticketno;
    // this.evenModel.totalticketAmt = this.evenModel.familyticketPrice * this.evenModel.ticketno;
    // this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "F" + "(" + this.showModel.room + ")" + ":-" + this.familyModel.setvar;
    // this.evenModel.type = "family";
    // this.evenModel.roomno = this.showModel.room;
    // this.evenModel.fno = this.familyModel.setvar;
    // this.totalTicketAmount();
    this.FamilysingleNumber();
    this.showno();
    // this.addLotteryNo();

  }

  changeColor(value: any) {
    console.log(value);
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[i][j].value == value) {
          console.log(this.modelSecond[i][j].value);
          this.modelSecond[i][j].status = 'yellow';

        }
      }
    }
  }

  // declare function overload(param: string);
  FamilysingleNumber() {
    let flag = 0;
    if (this.evenModel.value != -1) {
      if (this.familyModel.value != -1) {
        if (this.familyModel.value <= 9) {
          this.showset(0);
        }
        console.log('combination', this.comb);
        for (let i = 0; i < this.comb.set.length; i++) {
          this.len = this.comb.set[i];
          let len1 = this.comb.set[i];
          console.log('len', this.len);
          console.log('len1', len1);
        }
        for (let j in this.len) {
          let temp = Object.values(this.len)[j];
          if (temp == this.familyModel.value) {
            while (this.familyModel.value > 0) {
              this.familyModel.single = this.familyModel.value % 10;
              this.familyModel.value = Math.floor(this.familyModel.value / 10);
              console.log(this.familyModel.single);
              this.showset(this.familyModel.single);
              flag = 1;
              this.familyModel.value = 0;
              break;

            }
          }
          console.log('pair', j, temp);
        }
        if (flag != 1) {

          while (this.familyModel.value > 0) {
            this.familyModel.single = this.familyModel.value % 10;
            this.familyModel.value = Math.floor(this.familyModel.value / 10);
            console.log(this.familyModel.single);
            this.showset(this.familyModel.single);
          }
        }

        this.show1();
      } else {
        alert('select your number first');
      }


    }
    else {
      alert('Select your gher first');
    }
  }

  setmodel1: any = [];

  pushdata(value1: any) {
    this.setmodel1.push({
      "value": value1,
    });
  }

  show1() {
    console.log('can you see?');
    console.log(this.setmodel);
    for (let i = 0; i < this.setmodel.length; i++) {
      console.log('--family---');
      let len: any;
      len = this.setmodel[i].set;
      console.log(len);
      //console.log(len.length);
      console.log('i:-', i);
      for (let j in len) {
        console.log(Object.keys(len)[j], Object.values(len)[j]);
        this.changeModel.change = Object.values(len)[j];
        console.log('change color', this.changeModel.change);
        this.changeColor(this.changeModel.change);
        this.pushdata(this.changeModel.change);
      }
    }
    this.setmodel = [];

  }

  showset(_id: number) {
    console.log('showset');
    for (let i = 0; i < this.array.set.length; i++) {
      if (this.array.set[i].id == _id) {
        for (let j = 0; j < this.array.set[i].setlist.length; j++) {
          console.log('show one');
          console.log(this.array.set[i].setlist[j]);
          this.setmodel.push({
            "set": this.array.set[i].setlist[j],
          });

        }
      }
    }

    console.log(this.array);
  }


  setmodel: any = []


  // timetable = {
  //  "time" : [
  //    {
  //     0 : "10:15",
  //    }

  //  ]
  // }

  comb = {
    "set": [
      {
        0: "00",
        1: 11,
        2: 22,
        3: 33,
        4: 44,
        5: 66,
        7: 77,
        8: 88,
        9: 99,
        10: "05",
        11: 50,
        12: 16,
        13: 61,
        14: 27,
        15: 72,
        16: 38,
        17: 83,
        18: 49,
        19: 94,

      }
    ]
  }



  array : any = [
    {
      "value": 0,
      "numbersSelected": [
        "0",
        "5",
        "50",
        "55"
      ]
    },
    {
      "value": 1,
      "numbersSelected": [
        "11",
        "10",
        "15",
        "51",
        "06",
        "60",
        "65",
        "56",
      ]
    },
    {
      "value": 2,
      "numbersSelected": [
        "2",
        "20",
        "25",
        "52"
      ]
    },
    {
      "value": 3,
      "numbersSelected": [
        "3",
        "30",
        "35",
        "53",
        "58",
        "85",
        "08",
        "80",
      ]
    },
    {
      "value": 4,
      "numbersSelected": [
        "4",
        "40",
        "45",
        "54",
        "59",
        "95",
        "09",
        "90",
      ]
    },
    {
      "value": 5,
      "numbersSelected": [
        "0",
        "05",
        "50",
        "55",
      ]
    },
    {
      "value": 6,
      "numbersSelected": [
        "6",
        "60",
        "65",
        "56",
        "15",
        "51",
        "10",
        "1",
      ]
    },
    {
      "value": 7,
      "numbersSelected": [
        "7",
        "70",
        "75",
        "57",
         "25",
         "52",
         "20",
         "2"
      ]
    },
    {
      "value": 8,
      "numbersSelected": [
        "8",
        "80",
        "85",
        "58",
         "35",
         "53",
         "30",
         "3"
      ]
    },
    {
      "value": 9,
      "numbersSelected": [
         "9",
        "90",
        "95",
        "59",
         "45",
         "54",
         "40",
         "4"
      ]
    },{
      "value": 10,
      "numbersSelected": [
        "10",
        "1",
        "15",
        "51",
         "6",
         "60",
         "56",
         "65"
      ]
    },{
      "value": 11,
      "numbersSelected": [
        "11",
        "66",
        "16",
        "61",
      ]
    },{
      "value": 12,
      "numbersSelected": [
        "12",
        "21",
        "17",
        "71",
         "76",
         "67",
         "26",
         "62"
      ]
    },{
      "value": 13,
      "numbersSelected": [
        "13",
        "31",
        "18",
        "81",
         "86",
         "68",
         "63",
         "36"
      ]
    },{
      "value": 14,
      "numbersSelected": [
        "14",
        "41",
        "46",
        "64",
         "96",
         "69",
         "91",
         "19"
      ]
    },{
      "value": 15, // check
      "numbersSelected": [
        "15",
        "1",
        "51",
        "6",
         "60",
         "56",
         "65",
         
      ]
    },{
      "value": 16,
      "numbersSelected": [
        "16",
        "11",
        "66",
        "61",
      ]
    },{
      "value": 17,
      "numbersSelected": [
        "17",
        "12",
        "21",
        "71",
         "76",
         "67",
         "26",
         "62"
      ]
    },{
      "value": 18,
      "numbersSelected": [
        "18",
        "81",
        "13",
        "31",
         "86",
         "68",
         "63",
         "36"
      ]
    },{
      "value": 19,
      "numbersSelected": [
        "19",
        "91",
        "14",
        "41",
         "46",
         "64",
         "96",
         "69"
      ]
    },{
      "value": 20,
      "numbersSelected": [
        "20",
        "2",
        "25",
        "52",
         "57",
         "75",
         "7",
         "70"
      ]
    },{
      "value": 21,
      "numbersSelected": [
        "21",
        "12",
        "17",
        "71",
         "76",
         "26",
         "62",
         "67"
      ]
    },{
      "value": 22,
      "numbersSelected": [
        "22",
        "27",
        "72",
        "77",
      ]
    },{
      "value": 23,
      "numbersSelected": [
        "23",
        "32",
        "37",
        "73",
         "78",
         "87",
         "28",
         "82"
      ]
    },{
      "value": 24,
      "numbersSelected": [
        "24",
        "42",
        "47",
        "74",
         "92",
         "29",
         "97",
         "79"
      ]
    },{
      "value": 25,
      "numbersSelected": [
        "25",
        "52",
        "20",
        "2",
         "57",
         "75",
         "7",
         "70"
      ]
    },{
      "value": 26,
      "numbersSelected": [
        "26",
        "62",
        "17",
        "71",
         "12",
         "21",
         "76",
         "67"
      ]
    },{
      "value": 27,
      "numbersSelected": [
        "27",
        "72",
        "22",
        "77",
      ]
    },{
      "value": 28,
      "numbersSelected": [
        "28",
        "82",
        "23",
        "32",
         "37",
         "73",
         "78",
         "87"
      ]
    },{
      "value": 29,
      "numbersSelected": [
        "29",
        "92",
        "24",
        "42",
         "47",
         "74",
         "97",
         "79"
      ]
    },{
      "value": 30,
      "numbersSelected": [
        "30",
        "3",
        "35",
        "53",
         "58",
         "85",
         "8",
         "80"
      ]
    },{
      "value": 31,
      "numbersSelected": [
        "31",
        "13",
        "18",
        "81",
         "86",
         "68",
         "63",
         "36"
      ]
    },{
      "value": 32,
      "numbersSelected": [
        "32",
        "23",
        "37",
        "73",
         "78",
         "87",
         "28",
         "82"
      ]
    },{
      "value": 33,
      "numbersSelected": [
        "33",
        "88",
        "83",
        "38",
      ]
    },{
      "value": 34,
      "numbersSelected": [
        "34",
        "43",
        "84",
        "48",
         "93",
         "98",
         "89",
         "39"
      ]
    },{
      "value": 35,
      "numbersSelected": [
        "35",
        "30",
        "3",
        "53",
         "58",
         "85",
         "8",
         "80"
      ]
    },{
      "value": 36,
      "numbersSelected": [
        "36",
        "31",
        "13",
        "18",
         "31",
         "63",
         "86",
         "68"
      ]
    },{
      "value": 37,
      "numbersSelected": [
        "37",
        "73",
        "32",
        "23",
         "78",
         "87",
         "28",
         "82"
      ]
    },{
      "value": 38,
      "numbersSelected": [
        "38",
        "83",
        "33",
        "88",
      ]
    },{
      "value": 39,
      "numbersSelected": [
        "39",
        "93",
        "34",
        "43",
         "84",
         "48",
         "98",
         "89"
      ]
    },{
      "value": 40,
      "numbersSelected": [
        "40",
        "4",
        "45",
        "54",
         "59",
         "95",
         "9",
         "90"
      ]
    },{
      "value": 41,
      "numbersSelected": [
        "41",
        "14",
        "46",
        "64",
         "96",
         "91",
         "19",
         "69"
      ]
    },{
      "value": 42,
      "numbersSelected": [
        "42",
        "24",
        "29",
        "92",
         "47",
         "74",
         "97",
         "79"
      ]
    },{
      "value": 43,
      "numbersSelected": [
        "43",
        "34",
        "39",
        "93",
         "84",
         "48",
         "89",
         "89"
      ]
    },{
      "value": 44,
      "numbersSelected": [
        "49",
        "94",
        "99",
        "44",
       
      ]
    },{
      "value": 45,
      "numbersSelected": [
        "45",
        "54",
        "40",
        "0",
         "59",
         "95",
         "9",
         "90"
      ]
    },{
      "value": 46,
      "numbersSelected": [
        "46",
        "64",
        "41",
        "14",
         "96",
         "69",
         "91",
         "19"
      ]
    },{
      "value": 47,
      "numbersSelected": [
        "47",
        "74",
        "42",
        "24",
         "29",
         "92",
         "97",
         "79"
      ]
    },{
      "value": 48,
      "numbersSelected": [
        "48",
        "84",
        "43",
        "34",
         "39",
         "93",
         "98",
         "89"
      ]
    },{
      "value": 49,
      "numbersSelected": [
        "49",
        "94",
        "44",
        "99",
      ]
    },
    {
      "value": 50,
      "numbersSelected": [
        "50",
        "5",
        "55",
        "0",
      ]
    }, {
      "value": 51,
      "numbersSelected": [
        "51",
        "15",
        "1",
        "15",
        "6",
        "60",
        "56",
        "65",
      ]
    },
    {
      "value": 52,
      "numbersSelected": [
        "52",
        "25",
        "20",
        "2",
        "57",
        "75",
        "7",
        "70",
      ]
    },
    {
      "value": 53,
      "numbersSelected": [
        "53",
        "35",
        "30",
        "0",
        "58",
        "85",
        "8",
        "80",
      ]
    },
    {
      "value": 54,
      "numbersSelected": [
        "54",
        "45",
        "40",
        "4",
        "59",
        "95",
        "9",
        "90",
      ]
    },
    {
      "value": 55,
      "numbersSelected": [
        "55",
        "50",
        "5",
        "0",
      
      ]
    },
    {
      "value": 56,
      "numbersSelected": [
        "56",
        "65",
        "51",
        "15",
        "1",
        "6",
        "0",
        "60",
      ]
    },
    {
      "value": 57,
      "numbersSelected": [
        "57",
        "75",
        "52",
        "25",
        "20",
        "2",
        "7",
        "70",
      ]
    },
    {
      "value": 58,
      "numbersSelected": [
        "58",
        "85",
        "3",
        "30",
        "53",
        "35",
        "80",
        "8",
      ]
    },
    {
      "value": 59,
      "numbersSelected": [
        "59",
        "54",
        "45",
        "40",
        "4",
        "95",
        "9",
        "90"
      ]
    },
    {
      "value": 60,
      "numbersSelected": [
        "00",
        "05",
        "50",
        "55"
      ]
    },
    {
      "value": 60,
      "numbersSelected": [
        "60",
        "56",
        "51",
        "15",
        "1",
        "15",
        "6",
        "65"
      ]
    },
    {
      "value":61 ,
      "numbersSelected": [
        "61",
        "16",
        "11",
        "66"
      ]
    },
    {
      "value": 62,
      "numbersSelected": [
        "62",
        "26",
        "17",
        "12",
        "21",
        "71",
        "76",
        "67"
      ]
    },
    {
      "value": 63,
      "numbersSelected": [
        "63",
        "36",
        "31",
        "13",
        "18",
        "81",
        "86",
        "68"
      ]
    },
    {
      "value": 64,
      "numbersSelected": [
        "64",
        "46",
        "41",
        "14",
        "96",
        "69",
        "91",
        "19"
      ]
    },
    {
      "value": 65,
      "numbersSelected": [
        "65",
        "60",
        "56",
        "51",
        "15",
        "1",
        "15",
        "6"
      ]
    },
    {
      "value": 66,
      "numbersSelected": [
        "66",
        "61",
        "16",
        "11"
      ]
    },
    {
      "value": 67,
      "numbersSelected": [
        "67",
        "62",
        "26",
        "17",
        "12",
        "21",
        "71",
        "76"
      ]
    },
    {
      "value": 68,
      "numbersSelected": [
        "68",
        "63",
        "36",
        "31",
        "13",
        "18",
        "81",
        "86"
      ]
    },
    {
      "value": 69,
      "numbersSelected": [
        "69",
        "64",
        "46",
        "41",
        "14",
        "96",
        "91",
        "19"
      ]
    },
    {
      "value": 70,
      "numbersSelected": [
        "70",
        "7",
        "75",
        "57",
        "25",
        "52",
        "20",
        "2"
      ]
    },
    {
      
      "value": 71,
      "numbersSelected": [
        "71",
        "17",
        "76",
        "67",
        "21",
        "12",
        "62",
        "26",
      ]
    }, {
      "value": 72,
      "numbersSelected": [
        "72",
        "27",
        "77",
        "22",
      ]
    }, {
      "value": 73,
      "numbersSelected": [
        "73",
        "37",
        "28",
        "82",
        "78",
        "87",
        "32",
        "23",
      ]
    }, {
      "value": 74,
      "numbersSelected": [
        "74",
        "47",
        "29",
        "92",
        "79",
        "97",
        "24",
        "42"
      ]
    }, {
      "value": 75,
      "numbersSelected": [
        "75",
        "57",
        "20",
        "2",
        "70",
        "7",
        "25",
        "52"
      ]
    }, {
      "value": 76,
      "numbersSelected": [
        "76",
        "67",
        "12",
        "21",
        "71",
        "17",
        "26",
        "62",
      ]
    },
    {
      "value": 77,
      "numbersSelected": [
        "77",
        "22",
        "27",
        "72",
      ]
    },
    {
      "value": 78,
      "numbersSelected": [
        "78",
        "87",
        "23",
        "32",
        "73",
        "37",
        "82",
        "28",
      ]
    },
    {
      "value": 79,
      "numbersSelected": [
        "79",
        "97",
        "24",
        "42",
        "74",
        "47",
        "29",
        "92",
      ]
    },
    {
      "value": 80,
      "numbersSelected": [
        "80",
        "8",
        "35",
        "53",
        "85",
        "58",
        "30",
        "3",
      ]
    },
    {
      "value": 81,
      "numbersSelected": [
        "81",
        "18",
        "36",
        "36",
        "86",
        "68",
        "31",
        "13",
      ]
    },
    {
      "value": 82,
      "numbersSelected": [
        "82",
        "28",
        "37",
        "73",
        "23",
        "32",
        "87",
        "78",
      ]
    },
    {
      "value": 83,
      "numbersSelected": [
        "88",
        "33",
        "38",
        "83",
       
      ]
    },
    {
      "value": 84,
      "numbersSelected": [
        "84",
        "48",
        "39",
        "93",
        "89",
        "98",
        "43",
        "34",
      ]
    },
    {
      "value": 85,
      "numbersSelected": [
        "85",
        "58",
        "30",
        "3",
        "35",
        "53",
        "80",
        "8",
      ]
    },
    {
      "value": 86,
      "numbersSelected": [
        "86",
        "68",
        "31",
        "13",
        "18",
        "81",
        "63",
        "36",
      ]
    },
    {
      "value": 87,
      "numbersSelected": [
        "87",
        "78",
        "32",
        "23",
        "83",
        "28",
        "73",
        "37",
      ]
    },
    {
      "value": 88,
      "numbersSelected": [
        "88",
        "33",
        "38",
        "83",
      ]
    },
    {
      "value": 89,
      "numbersSelected": [
        "89",
        "98",
        "34",
        "43",
        "84",
        "48",
        "39",
        "93",
      ]
    },
    {
      "value": 90,
      "numbersSelected": [
        "90",
        "9",
        "45",
        "54",
        "40",
        "4",
        "95",
        "59",
      ]
    },
    {
      "value": 91,
      "numbersSelected": [
        "91",
        "19",
        "46",
        "64",
        "96",
        "69",
        "41",
        "14",
      ]
    },
    {
      "value": 92,
      "numbersSelected": [
        "92",
        "29",
        "47",
        "74",
        "97",
        "79",
        "42",
        "24",
      ]
    },
    {
      "value": 93,
      "numbersSelected": [
        "93",
        "39",
        "48",
        "84",
        "98",
        "89",
        "34",
        "43",
      ]
    },
    {
      "value": 94,
      "numbersSelected": [
        "94",
        "49",
        "44",
        "99",
      ]
    },
    {
      "value": 95,
      "numbersSelected": [
        "95",
        "59",
        "40",
        "4",
        "90",
        "9",
        "45",
        "54",
      ]
    },
    {
      "value": 96,
      "numbersSelected": [
        "96",
        "69",
        "14",
        "41",
        "19",
        "91",
        "46",
        "64",
      ]
    },
    {
      "value": 97,
      "numbersSelected": [
        "97",
        "79",
        "42",
        "24",
        "92",
        "29",
        "47",
        "74",
      ]
    },
    {
      "value": 98,
      "numbersSelected": [
        "98",
        "89",
        "43",
        "34",
        "93",
        "39",
        "48",
        "84",
      ]
    },
    {
      "value": 99,
      "numbersSelected": [
        "99",
        "44",
        "49",
        "94",
      
      ]
    },
  ] 
    
  

  // family end --------------************************-----------------------------------

  createSecondModel() {
    this.modelSecond = [];
    let tempArray1 = [];
    for (var i = 0; i < 100; i++) {
      if (i <= 9) {
        let temp: string;
        tempArray1.push({
          "value": i,
          "name": '0' + i,
          "status": "inactive"
        });
      }
      else {
        tempArray1.push({
          "value": i,
          "name": i,
          "status": "inactive"
        });
      }
      if ((i + 1) % 10 == 0) {
        this.modelSecond.push(tempArray1);
        tempArray1 = [];
      }
    }

  }


  disablegher(_value: any) {
    this.gherModel.gher = _value;
    for (let i = 0; i <= this.modelFirst.length - 1; i++) {
      for (let j = 0; j <= this.modelFirst[i].length - 1; j++) {
        if (this.gherModel.gher == this.modelFirst[i][j].value) {
          this.modelFirst[i][j].status = 'inactive';
        }

      }
    }
  }

  inactiveall() {
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        this.modelSecond[i][j].status = 'inactive';
      }
    }
    for (let i = 0; i <= this.modelFirst.length - 1; i++) {
      for (let j = 0; j <= this.modelFirst[i].length - 1; j++) {
        this.modelFirst[i][j].status = 'inactive';
      }
    }
  }

  inactive() {
    for (let element of  this.modelFirst) {
      element.status='inactive';
      
    }
  }

  inactiveOdd() {
    for (let i = 0; i <= this.modelSecond.length - 1; i++) {
      for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
        if (this.modelSecond[i][j].value % 2 != 0) {
          this.modelSecond[i][j].status = 'inactive';
        }
      }
    }
  }

  evenShow() {
    this.flag = 0;
    if (this.evenModel.value != -1) {
      for (let i = 0; i < this.billModel.length; i++) {
        let temp = "Gher" + "(" + this.showModel.value + ")" + "Even";
        if (temp == this.billModel[i].gher) {
          this.evenModel.ticketno = 0;
          alert('you have already selected');
          this.flag = 1;
        }
      }
      if (this.flag != 1) {
        for (let i = 0; i <= this.modelSecond.length - 1; i++) {
          for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
            if (this.modelSecond[i][j].value % 2 == 0) {
              console.log(this.modelSecond[i][j].value);
              this.modelSecond[i][j].status = 'active';
              this.evenModel.ticket++;
              this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Even";
              this.evenModel.type = "even";
            }
          }
        }
        this.addLotteryNo();

      }
      console.log(this.evenModel.ticket);
    }
    else {
      alert('select your gher first');
    }
  }

  // odd--------------------********************************************---------------------------------------------------

  OddShow() {
    this.flag = 0;
    if (this.evenModel.value != -1) {
      for (let i = 0; i < this.billModel.length; i++) {
        let temp = "Gher" + "(" + this.evenModel.value + ")" + "Odd";
        if (temp == this.billModel[i].gher) {
          this.evenModel.oddticketno = 0;
          alert('you have already selected');
          this.flag = 1;
        }
      }
      if (this.flag != 1) {
        for (let i = 0; i <= this.modelSecond.length - 1; i++) {
          for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
            if (this.modelSecond[i][j].value % 2 != 0) {
              console.log(this.modelSecond[i][j].value);
              this.modelSecond[i][j].status = 'active';
              this.evenModel.ticket++;
              this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Odd";
            }
          }
        }
        this.addLotteryNo();
      }
      console.log(this.evenModel.ticket);
    }
    else {
      alert('select your gher first');
    }
  }

  //------------------------********************************************---------------------------------------------------

  //** Double  **/
  //----------------Right-----------------------

  right() {
    this.flag = 0;
    let eo = 0;
    let even = 0;
    let odd = 0;
    if (this.evenModel.value != -1) {
      let temp = "Gher" + "(" + this.evenModel.value + ")" + "Right";
      let temp1 = "Gher" + "(" + this.evenModel.value + ")" + "Odd";
      let temp2 = "Gher" + "(" + this.evenModel.value + ")" + "Even";
      for (let i = 0; i < this.billModel.length; i++) {
        if (temp == this.billModel[i].gher) {
          alert('you have already selected');
          this.evenModel.leftticketno = 0;
          this.flag = 1;
        }
        //   for(let j=0;j<this.billModel.length;j++)
        // {
        //   //odd
        //   if(temp1 == this.billModel[j].gher)
        //     {
        //       for(let k=0;k<this.billModel.length;k++)
        //     {
        //       if(temp2 == this.billModel[k].gher)
        //     {
        //       //even
        //         alert('even & odd already selected in this gher');
        //         this.flag=1;
        //         even=1;
        //         this.evenModel.ticketno=0
        //         this.evenModel.leftticketno=0;
        //     }

        //     }
        //     //function
        //     }
        //   }
        //   for(let j=0;j<this.billModel.length;j++)
        // {
        //   if(temp2 == this.billModel[j].gher)
        //     {
        //       //even
        //       for(let k=0;i<this.billModel.length;k++)
        //     {
        //       if(temp1 == this.billModel[k].gher)
        //     {
        //       //odd
        //         alert('even & odd already selected in this gher');
        //         this.flag=1;
        //         this.evenModel.ticketno=0;
        //         this.evenModel.leftticketno=0;
        //     }
        //     }
        //     }
        //   }
      }
      if (this.flag != 1) {
        let nine = 9;
        let nine1: number;
        for (let n = 1; n <= 10; n++) {
          nine1 = nine * n;

          for (let i = 0; i <= this.modelSecond.length - 1; i++) {
            for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {

              if (nine1 == this.modelSecond[i][j].value) {
                console.log(this.modelSecond[i][j].value);
                this.modelSecond[i][j].status = 'active';
                // this.evenModel.ticket++;
                this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Right";
                this.evenModel.type = "right"
              }
            }
          }
        }
        this.addLotteryNo();
        this.gherchangeSingleColor(this.evenModel.value);
      }

    }
    else {
      alert('select your gher first');
    }
  }


  //--------------------------------------------
  double() {
    this.flag = 0;
    let eo = 0;
    let even = 0;
    let odd = 0;
    if (this.evenModel.value != -1) {
      let temp = "Gher" + "(" + this.evenModel.value + ")" + "Left";
      let temp1 = "Gher" + "(" + this.evenModel.value + ")" + "Odd";
      let temp2 = "Gher" + "(" + this.evenModel.value + ")" + "Even";
      for (let i = 0; i < this.billModel.length; i++) {
        if (temp == this.billModel[i].gher) {
          alert('you have already selected');
          this.evenModel.leftticketno = 0;
          this.flag = 1;
        }

      }
      if (this.flag != 1) {
        for (let i = 0; i <= this.modelSecond.length - 1; i++) {
          for (let j = 0; j <= this.modelSecond[i].length - 1; j++) {
            if (this.modelSecond[i] == this.modelSecond[j]) {
              console.log(this.modelSecond[i][j].value);
              this.modelSecond[i][j].status = 'active';
              // this.evenModel.ticket++;
              this.showModel.temp = "Gher" + "(" + this.showModel.value + ")" + "Left";
              this.evenModel.type = "left"
            }
          }
        }
        this.addLotteryNo();
        this.gherchangeSingleColor(this.evenModel.value);
      }

    }
    else {
      alert('select your gher first');
    }
  }

  evendouble() {
    // if(even==1)
    //       {
    for (let m = 0; m <= this.modelSecond.length - 1; m++) {
      for (let n = 0; n <= this.modelSecond[m].length - 1; n++) {
        if (this.modelSecond[m][n].value % 2 != 0) {
          if (this.modelSecond[m] == this.modelSecond[n]) {
            console.log(this.modelSecond[m][n].value);
            this.modelSecond[m][n].status = 'change';
          }
        }
      }
    }
    // }
  }

  numberArray = [
    {
      "gher": 0,
      "room": 0,
      "value": 0,
      "family": 0
    }
  ]

  selectThisNumber(input: any) {

    this.billModel.push({
      "gherno": input,
      "values": []
    });
  }

  addLotteryNo() {
    this.billModel.push({
      "gherno": this.evenModel.value,
      "gher": this.showModel.temp,
      "ticketno": this.evenModel.ticketno,
      "totalticketAmt": this.evenModel.totalticketAmt,
      "type": this.evenModel.type,
      "backno": this.evenModel.backno,
      "fno": this.evenModel.fno,
      "roomno": this.evenModel.roomno,
    });
    this.evenModel.ticketno = 0;
    this.showModel.temp = "";
    this.evenModel.totalticketAmt = 0;
    this.IsVisible = false;
    this.totalTicketAmount();
    this.familyModel.setvar = "";
    this.setmodel1 = [];
    this.showModel.temp = "";
    this.familyModel.ticketno = 0;
    this.evenModel.oddticketno = 0;
    this.evenModel.leftticketno = 0;
    this.evenModel.rightticketno = 0;
    this.evenModel.type = "";
    //let temp = this.billModel[this.billModel-1];
    let temp = this.billModel[this.billModel.length - 1];
    console.log('temp', temp);
    this.multModel.gherno = temp.gherno;
    this.multModel.showgherno = temp.gherno;
    this.multModel.flag = 0;
    this.gettotal();

  }

isdeletevalue = false;
truevalue(input){
  this.isdeletevalue = input;
}
  delete(_gherno: any, selectedItem: any , ) {
    
    
     
     if( this.billModel[_gherno].values.length==0)
     {
        console.log('this.is empty');
     }
    
      let tempArray = [];
      for (let val in this.billModel[_gherno].values) {
        if (val != selectedItem) {
          tempArray.push(this.billModel[_gherno].values[val]);
        }
      }
      this.billModel[_gherno].values = tempArray;
      if( this.billModel[_gherno].values.length==0)
      {
        this.billModel.splice(_gherno, 1);
        console.log('this.is empty');
      }
    // }
    // this.isdeletevalue = false;
    this.isdeletevalue = false;
    this.gettotal();
    console.log(this.billModel);

   // this.selectedNumber(this.evenModel.value.value);
    
  }

  minus(selectedItem: any) {
    console.log("Selected item Id: ", selectedItem.gher);
    // this.gettotal();
    if (selectedItem.ticketNo > 0) {
      if (selectedItem.type == "single") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice;
      }
      else if (selectedItem.type == "left") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 10;
      }
      else if (selectedItem.type == "right") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 10;
      }
      else if (selectedItem.type == "family") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 8;
      }
      else if (selectedItem.type == "back") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 10;
      }
      else if (selectedItem.type == "front") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 10;
      } else if (selectedItem.type == "even") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 50;
      } else if (selectedItem.type == "odd") {
        selectedItem.ticketNo = selectedItem.ticketNo - 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt - this.getPriceModel.singlePrice * 50;
      }

      this.gettotal();
    }
    else {
      alert('Balance is low');
    }
  }

  plus(selectedItem: any) {
    console.log("Selected item Id: ", selectedItem.gher);
    // this.gettotal();
    if (selectedItem.ticketNo > 0) {
      if (selectedItem.type == "single") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice;
      }
      else if (selectedItem.type == "left") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice * 10;
      }
      else if (selectedItem.type == "right") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice * 10;
      }
      else if (selectedItem.type == "family") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt +this.getPriceModel.singlePrice * 80;
      }
      else if (selectedItem.type == "back") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice * 10;
      }
      else if (selectedItem.type == "front") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice * 10
      } else if (selectedItem.type == "even") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice * 50;
      } else if (selectedItem.type == "odd") {
        selectedItem.ticketNo = selectedItem.ticketNo + 1;
        selectedItem.totalticketAmt = selectedItem.totalticketAmt + this.getPriceModel.singlePrice * 50;
      }

      this.gettotal();
    }
  }

  multigher(_value: any) {
    if (_value == true) {
      console.log('check mukti gher true');

    } else {
      console.log('check multi gher false');
      let temp = this.billModel[this.billModel.length - 1];
      console.log(temp);
      this.multModel.flag = 1;
      this.multighrclone();
    }
    console.log(this.checkboxModel.value);
  }

  // total Amount

  //
  getcurrentSlot(){
     this.checktime();
    for(let book of this.bookingslot)
    {
      
        if(this.matchtime==book.startTime)
        {
          this.slots.push({
          "endTime": book.endTime,
          "startTime" : book.startTime,
           "id" : book.id,
           "status" : book.status,

          })
        
      }
     
    }
    console.log('current slot',this.matchtime);
    console.log('check currwnt slot',this.slots);
  }
  resultList: any=[];


  getresult(){
    console.log('chek result')
    this.lotteryHttpService.makeRequestApi('post', 'getAllResult', {bookingDate: this.datePipe.transform(new Date(),"dd-MM-yyyy")}).subscribe(
      res => {
            this.resultList=res;
            console.log('getresult api',this.resultList);
       });
  }

amount : any;
amtvalue : any;
  getAmount(){
    console.log('chek result')
    this.lotteryHttpService.makeRequestApi('post', 'getAmount', {requestBy: {
id : this.retailerID
    }  }).subscribe(
      res => {
            this.amount=res;
            console.log('getAmount',this.amount);
            this.amtvalue = this.amount.content.amount;
            console.log('getAmount',this.amtvalue);

       });
  }

  getslotlist(){
    this.lotteryHttpService.makeRequestApi('get', 'slotlist').subscribe(slot => {
      if (slot !=null) {
      this.bookingslot= slot;
      console.log('booking slot ali', this.bookingslot);
      this.getcurrentSlot();
      this.diable();
      }
      else{
        console.log('error');
      }
  
    });
  }
  price(){
    this.lotteryHttpService.makeRequestApi('get', 'priceList').subscribe(price => {
      if (price !=null) {
        console.log(price);
        const pricecopy = price;
        this.storageService.set('ticketPrice', pricecopy);
        console.log('price',pricecopy);
      }
      else{
        console.log('error');
      }
  
    });
  }

  getPrice : any;
  getPriceModel : any ={
    "singlePrice" :0,
    "leftPrice"   :0,
    "rightprice"   :0,
    "evenPrice"   :0,
    "oddPrice"   :0,
    "frontPrice"   :0,
    "backPrice"   :0,
    "familyPrice" :0,
    "allPrice" :0,
  }
  fetchprice(){
    this.getPrice=  this.storageService.get('ticketPrice');

    for(let pr of this.getPrice){
        if(pr.name=='single')
        {
          this.getPriceModel.singlePrice= pr.price;
        }
    }
    this.getPriceModel.leftPrice  =  this.getPriceModel.singlePrice * 10;
    this.getPriceModel.rightPrice =  this.getPriceModel.singlePrice * 10;
    this.getPriceModel.evenPrice  =  this.getPriceModel.singlePrice * 50;
    this.getPriceModel.oddPrice   =  this.getPriceModel.singlePrice * 50;
    this.getPriceModel.frontPrice =  this.getPriceModel.singlePrice * 10;
    this.getPriceModel.backPrice  =  this.getPriceModel.singlePrice * 10;
    this.getPriceModel.familyPrice  =  this.getPriceModel.singlePrice * 8;
    this.getPriceModel.allPrice  =  this.getPriceModel.allPrice * 100;
    console.log('single price',this.getPriceModel.singlePrice);
  }
  // singprice= this.singleprice[0].price;
  // sinname = this.singleprice[0].name;

  
  callback() {
    let cdate = new Date();
    cdate.setHours(cdate.getHours()-2);
    this.countupTimerService.startTimer(cdate);
    console.log('sdsd');
   // console.log( this.countupTimerService.startTimer(cdate));
  }




  counter: number = 0;
  timerId: string;
  ngOnInit() {
    this.getAmount();
    
this.minTime();
console.log('family array',this.array);
    $(".toggle-password").click(function() {

      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
   // this.callback();

    // this.startTimer();
    this.createFirstModel();
    this.createSecondModel();
    this.totalTicketAmount();
    this.show1();
    this.front();
    this.time();
   
    console.log('userDetails',this.userD);
    // console.log('userDetails', this.userD);
    // console.log('name', this.sinname);
    // console.log('singpr', this.singprice);
    // console.log('prc', this.singleprice);
    this.gettotal();
     this.price();
    this.getslotlist();
   
    this.fetchprice();
    this.captureScreen();
    this.checktime();
    this.diable();
    this.getresult();
    this.getBalance();
    // this.checktime();
    // this.getcurrentSlot();
    setInterval(() => {
      this.checktime(); 
      }, 1000);

    //   setInterval(() => {
    //     this.diable(); 
    //     }, 1000);

       setInterval(() => {
        this.countdown();
        }, 1000);

      


  }

  gettotal() {
    let total = 0;
    this.totalModel.total = 0;
    for (let _element of this.billModel) {
      for (let element1 of _element.values) {
        this.totalModel.total = this.totalModel.total + element1.totalticketAmt;
      }
    }
    console.log(total);
  }

  ctrlModel: any = {
    "number": false,
  }
  numb: any = false;
  gherarray: any = [
    {
      "gherno": -1,
      "status": 2,
    }
  ];

  slots : any = [
  ]


  existInSlots(input){
    let exist = false;
    for(let slot of this.slots){
      if(slot.id == input.id ) exist = true; 
    }
    return exist;
  }
  addtime(input: any) {
    let flag=0;
    for(let slot of this.slots)
    {
      if(input==slot)
      {
        this.slots.splice(input, 1);
        flag=1;
      }
    }
    if(flag==0){
      this.slots.push(input);  
    }
     console.log(this.slots); 

        }

  isKeyPressed(event, input) {

    let flag = 0;
    if (event.ctrlKey) {
      this.ctrlModel.number = true;
      flag = 1;
    } else {
      flag = 2;
    }
    if (flag == 1) {
      let check = 0;
      for (let gh of this.gherarray) {
        if (gh.gherno == input.value) {
          gh.status = 2;
          check = 1;
        }
      }
      if (check == 0) {
        this.gherarray.push({
          "gherno": input.value,
          "status": 1,
        });
      }

      for (let ctrl of this.gherarray) {
        for (let element of this.modelFirst) {
          for (let element1 of element) {
            if (ctrl.gherno == element1.value) {
              if (ctrl.status == 2)
                element1.status = 'inactive';
            }
          }
        }
      }

      console.log(this.gherarray);
      console.log(this.ctrlModel.number);
    }
    for (let ctrl of this.gherarray) {
      for (let element of this.modelFirst) {
        for (let element1 of element) {
          if (ctrl.gherno == element1.value) {
            if (ctrl.status == 1)
              element1.status = 'active';
          }
        }
      }
    }
    console.log(this.gherarray);
    if (flag == 2) {
      console.log('try again');
      console.log('chgcgh', input);
    }

  }

  logKey(e: any) {
    console.log(e.ctrlKey);
    let temp = e.ctrlKey;
    this.numb = temp;
    console.log('ctrltemp', temp);
    console.log('ctrltemp123', this.numb);

    if (e.ctrlKey == true) {
      // this.ctrlModel.number=true;
      alert('ctrl key');
    }
    else {
      alert('not a ctrl key ');
    }
  }

  showkey(input: any) {
    if (input == true) {
      console.log('fgfc');
      console.log(this, this.ctrlModel.number);
      console.log(this, this.ctrlModel.number.value);
    }

  }

  refresh(): void {
    window.location.reload();
  }

 
previoustrans =0;
  allresult: any;
  saverepo() {
    this.getcurrentSlot();
    let input = {
      "slots": this.slots,
      "bookingDetail": {
        "distributorId": 0,
        "retailerId": this.retailerID,
        "totalPrice": this.totalModel.total,
      },
      "tickets": this.billModel
    }


    console.log(JSON.stringify(input));
    // let output=JSON.stringify(input);

    this.lotteryHttpService.makeRequestApi('post', 'booking', input).subscribe((res) => {

        this.alertService.swalSuccess("booking saved");
        console.log(res);
        this.allresult=res;
        // this.refresh();
        console.log('check data',this.allresult);
        for(let last of this.allresult){
          this.lastid= last.id;
        }
      //  this.ticketprint();
        this.billModel=[];
        this.previoustrans=this.totalModel.total;
        this.totalModel.total=0;
        this.slots=[];
        
      this.inactiveall();

    });
this.getAmount();

console.log('last id',this.lastid);

  }

lastid : any;
  public myAngularxQrCode: string = null;
match :any;
matchtime : any;
matchEndtime: any;
leftslot =1;
  checktime(){
    //this.leftslot= this.countslot;
    this.today;
    let h :any;
    let m :any;
    var d = new Date();
   h = d.getHours();
   m = d.getMinutes()
   console.log('time');
   console.log(h);
   console.log(m);
   this.match= h+":"+m;
  //  this.match= "19:00";
   for(let book of this.bookingslot)
   {
     if(this.match>=book.startTime && this.match<=book.endTime)
     {
              this.matchtime=book.drawTime;

              this.matchEndtime=book.drawTime;
              
     }
   }


  this.restslot();
  
    }

totalslot: any;
    restslot(){
         this.leftslot= this.countslot;
         this.totalslot= this.countslot;
      for(let book of this.bookingslot)
      {
        if(this.matchtime<=book.startTime)
        {
          this.leftslot--;
          //this.totalslot= this.leftslot;
        }
      } 
    }




    timeLeft: number = 60;
    min: number = 15;
    interval;
    startTimer() {
this.interval = () => {
  if(this.min > 0) {
    
  }
}
     this.interval = setInterval(() => {
        if(this.timeLeft > 0) {
          this.timeLeft--;
        } else {
          this.timeLeft = 60;
        }
      },1000)
    }
  //   onPrint(){
  //     window.print();
  // }

//   printToCart(printSectionId: string){
//     let popupWinindow
//     let innerContents = document.getElementById(printSectionId).innerHTML;
//     popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
//     popupWinindow.document.open();
//     popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
//     popupWinindow.document.close();
// }
    countslot: any =1;
    diable(){
      this.countslot= this.bookingslot.length;
    }  
  ticketprint(){
    this.randomnumber= Math.random();
    this.randomnumber= "DestributerID"+"Retailer ID"+" "+this.randomnumber;
this.myAngularxQrCode = this.randomnumber;
console.log(this.myAngularxQrCode);
this.modal2.show();

        

  }
  public captureScreen() {
    const data = document.getElementById('printSectionId');
    html2canvas(data).then(canvas => {
    const imgWidth = 50;
    const pageHeight = 50;
    const imgHeight = canvas.height * imgWidth / canvas.width;
    const heightLeft = imgHeight;
    const contentDataURL = canvas.toDataURL('image/png');
    const pdf = new jspdf('p', 'mm', 'a4'); 
    const position = 0;
    pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
    pdf.save('invoice.pdf'); 
    });
    this.billModel=[];
        this.totalModel.total=0;
        this.slots=[];
        this.modal2.hide();
    }

    ///

luckywinners = [
  {

    "id": 1,
    "gherno" : 22,
    "data" : [
      {
        "time" : "10:15",
         "no"  : 24,
      }   
    ],

  }
]

  // timer-------------
  getLuckyArray: any= [];
  clonearray: any= [];
  getreverseLuckyArray: any= [];
  getreverse(){
    let value =0;
    this.getreverseLuckyArray= [];
    for(let i=this.getLuckyArray.length-1;i>=0;i--)
    {
    
  this.getreverseLuckyArray.push({
    
    "gherno" :this.getLuckyArray[i].gherno,
     "luckyno" : this.getLuckyArray[i].luckyno,
     "drawtime" :this.getLuckyArray[i].drawtime,
   
  })
  
  value=0;
  
    }
    console.log('checkreverse',this.getreverseLuckyArray);
    
  }

  getluckyNumber(gherno: any){

    for(let res of this.resultList){
  if(gherno.value==res.gherNo){
  this.getLuckyArray.push({
    "gherno" : res.gherNo,
     "luckyno" : res.luckyNumber,
     "drawtime" : res.slotMaster.endTime,
  });
  }
    }
  
    console.log('check lucky result',this.getLuckyArray);
    this.getreverse();
  }
  //--------------------
  mint =15;
  minTime(){
    if(this.mint>0)
    { 
      // this.mint--;
    // this.countTimer();
    }
  }

  Lefttime: number = 60;
  interval1;

countTimer() {
    this.interval1 = setInterval(() => {
      if(this.Lefttime > 0) {
        this.Lefttime--;
      } else {
        this.Lefttime = 60;
      }
    },1000)
    this.mint--;
  }
  
getBalance(){
    let user= { 
      "userName": this.userD.user.userName, 
      "email": this.userD.user.email, 
      "userId":this.userD.user.id
     }
    this.http.post(this.url+'users/balance',user).subscribe(response => {
      this.balanceInfo = JSON.parse(JSON.stringify(response));
    });
}



  
  //----------------------
}