import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimTransactionComponent } from './claim-transaction.component';

describe('ClaimTransactionComponent', () => {
  let component: ClaimTransactionComponent;
  let fixture: ComponentFixture<ClaimTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
