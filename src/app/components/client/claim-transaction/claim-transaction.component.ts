import { Component, OnInit, ViewChild } from '@angular/core';
import * as $ from '../../../../../node_modules/jquery';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';

@Component({
  selector: 'app-claim-transaction',
  templateUrl: './claim-transaction.component.html',
  styleUrls: ['./claim-transaction.component.scss']
})
export class ClaimTransactionComponent implements OnInit {

  hideClaim = false;
  mprice: any;
  homeClass = "active";
  transactionClass = "inactive";
  dispatchClass = "inactive";
  homeActive = "btn-btn-primary";

  @ViewChild('modal1') modal1: any;
  @ViewChild('modal4') modal4: any;
  mobile: Number;
  ticket: Number;
  claimList: any = [];
  slotList: any = [];
  cliamDetailsList: any = [];
  claimedList: any = [];
  requestedDetails: any = [];
  dispatchedList: any = [];
  dispatchedDetail: any = [];
  transactionList: any = [];
  allTransactionList: any = [];
  tempMobile: Number;
  tempTicket: Number;
  requestList: any = [];
  requestedList: any = [];
  status: String;
  requestedId = this.storageService.get('currentUser').user.roleMaster.id
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber;
  phoneDisp = "(" + this.phoneNumber + ")";
  role = this.userD.user.roleMaster.name;
  property = false;
  roleId = this.userD.user.roleMaster.id;
  constructor(private sweetAlert: SweetAlertService, private storageService: LotteryStorageService, private lotteryHttpService: LotteryHttpService, private alertService: SweetAlertService, ) {
  }

  clicked() {
    this.tempMobile = this.mobile;
    this.tempTicket = this.ticket;
  }
  navModel: any = {
    "claim": 2,
    "transcation": 1,
    "dispatch": 1,
    "requested": 1,
  }
  navbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel.transcation = 2;
      this.navModel.claim = 1;
      this.navModel.dispatch = 1;
      this.navModel.requested = 1;
    }
    else if (input == 2) {
      this.navModel.transcation = 1;
      this.navModel.claim = 2;
      this.navModel.dispatch = 1;
      this.navModel.requested = 1;
    }
    else if (input == 3) {
      this.navModel.transcation = 1;
      this.navModel.claim = 1;
      this.navModel.dispatch = 2;
      this.navModel.requested = 1;
    }
    else if (input == 4) {
      this.navModel.transcation = 1;
      this.navModel.claim = 1;
      this.navModel.dispatch = 1;
      this.navModel.requested = 2;
    }
  }
  bookingDetails: any = {
    "id": "",
    "startTime": "",
    "totalPrice": "",
    "bookingDate": "",
  }
  getdetails() {
    $('.table').show();
    console.log(this.ticket);
    let reqMap = {
      id: this.ticket
    }
    id: this.ticket;
    this.lotteryHttpService.makeRequestApi('post', 'searchByNumber', reqMap).subscribe((res) => {
      this.claimList = res;
      // console.log('sttaus', this.claimList.status.id);
      this.bookingDetails.startTime = this.claimList.bookingDetail.slot.startTime;
      this.bookingDetails.id = this.claimList.bookingDetail.id;
      this.bookingDetails.totalPrice = this.claimList.bookingDetail.totalPrice;
      this.bookingDetails.bookingDate = this.claimList.bookingDetail.bookingDate;
      this.bookingDetails.status = this.claimList.status.id;
    
    });
  }
  ngOnInit() {

    console.log(this.requestedId);
    this.items = Object.keys(this.users[0]);
    this.persons = Object.keys(this.users);
  }
  allTransactionEntry: any = {
    "id": "",
    "name": "",
    "startTime": "",
    "totalPrice": "",
    "bookingDate": "",
    "status": "",

  }
  transList: any = [];
  openTransactionForm() {
    $('.claim-Form').hide();
    $('.transaction-Form').show();
    $('.dispatched-Form').hide();
    $('.requested-Form').hide();
    let reqMap = {

      requestBy: {
        id: this.storageService.get('currentUser').user.id,
      }
    }

    this.lotteryHttpService.makeRequestApi('post', 'viewAllTransactionClaim', reqMap).subscribe((res) => {
      this.transactionList = res;

      this.transList = this.transactionList.content;
      console.log(this.transList);
      for (var j = 0; j < this.transList.length; j++) {
        this.allTransactionList = this.transList[j]

        console.log(this.allTransactionList.claim);
        if (this.allTransactionList.claim != null) {
          console.log('hello');
          this.allTransactionEntry.startTime = this.allTransactionList.claim.bookingDetail.slot.startTime;
          this.allTransactionEntry.id = this.allTransactionList.claim.bookingDetail.id;
          this.allTransactionEntry.totalPrice = this.allTransactionList.claim.bookingDetail.totalPrice;
          this.allTransactionEntry.bookingDate = this.allTransactionList.claim.bookingDetail.bookingDate;
          this.allTransactionEntry.status = this.allTransactionList.status.name;
          this.allTransactionEntry.name = this.allTransactionList.requestBy.firstName;
        }
      }
    });
  }

  openCliamForm() {
    $('.transaction-Form').hide();
    $('.dispatched-Form').hide();
    $('.claim-Form').show();
    $('.requested-Form').hide();
  }
  dispatchedEntry: any = {
    "id": "",
    "name": "",
    "startTime": "",
    "totalPrice": "",
    "bookingDate": "",
    "status": "",

  }
  newList: any = [];
  openDispatchedForm() {
    $('.claim-Form').hide();
    $('.transaction-Form').hide();
    $('.dispatched-Form').show();
    $('.requested-Form').hide();
    let reqMap = {

      loginId: this.storageService.get('currentUser').user.id,
    }

    this.lotteryHttpService.makeRequestApi('post', 'viewDispatchClaim', reqMap).subscribe((res) => {

      this.newList = res.content;

      for (var j = 0; j < this.newList.length; j++) {
        this.dispatchedList = this.newList[j]
      }
      console.log(this.dispatchedList);
      // this.dispatchedEntry.startTime = this.dispatchedList.bookingDetail.slot.startTime;
      // this.dispatchedEntry.id = this.dispatchedList.bookingDetail.id;
      // this.dispatchedEntry.totalPrice = this.dispatchedList.bookingDetail.totalPrice;
      // this.dispatchedEntry.bookingDate = this.dispatchedList.bookingDetail.bookingDate;
      // this.dispatchedEntry.status = this.dispatchedList.status.name;
      // this.dispatchedEntry.name = this.dispatchedList.requestBy.firstName;
    })
  }
  requestedEntry: any = {
    "id": "",
    "name": "",
    "startTime": "",
    "totalPrice": "",
    "bookingDate": "",
    "status": "",

  }
  openRequestedForm() {
    $('.claim-Form').hide();
    $('.transaction-Form').hide();
    $('.dispatched-Form').hide();
    $('.requested-Form').show();
    let reqMap = {

      loginId: this.storageService.get('currentUser').user.id,
    }
    console.log(reqMap);
    this.lotteryHttpService.makeRequestApi('post', 'viewRequestClaim', reqMap).subscribe((res) => {
      this.requestList = res.content;

      for (var j = 0; j < this.requestList.length; j++) {
        this.requestedList = this.requestList[j]
      }
      console.log(this.requestedList);
      // this.requestedEntry.startTime = this.requestedList.bookingDetail.slot.startTime;
      // this.requestedEntry.id = this.requestedList.bookingDetail.id;
      // this.requestedEntry.totalPrice = this.requestedList.bookingDetail.totalPrice;
      // this.requestedEntry.bookingDate = this.requestedList.bookingDetail.bookingDate;
      // this.requestedEntry.status = this.requestedList.status.name;
      // this.requestedEntry.name = this.requestedList.requestBy.firstName;
    });

  }

  selfClaim(emp) {

    console.log(this.requestedId);
    let reqMap = {
      id: this.claimList.id,
      loginId: this.storageService.get('currentUser').user.id,
      paymentType: "cash"
    }

    this.lotteryHttpService.makeRequestApi('post', 'settledCliam', reqMap).subscribe((res) => {
    });
    this.modal1.hide();
    this.sweetAlert.swalSuccess('claim successfully Done');
  }



  farwordClaim(emp) {

    console.log(this.ticket);
    let reqMap = {
      id: this.claimList.id,
      loginId: this.storageService.get('currentUser').user.id,
      requestTo: {
        id: this.requestedId,
      }
    }
    console.log();
    console.log(reqMap);
    this.lotteryHttpService.makeRequestApi('post', 'requestClaim', reqMap).subscribe((res) => {
    });
    this.modal4.hide();
    this.sweetAlert.swalSuccess('claim successfully Farworded');

  }
  //   refresh(): void {
  //     window.location.reload();
  // }
  items: any = [];
  persons: any = [];
  users = [
    { "name": "Anil", "mobile": 123, "ticket": 222, "date": "12-05-2019", "status": "Requested", "Amount": 1250234, "settled_by": " " },
    { "name": "Reena", "mobile": 321, "ticket": 111, "date": "12-05-2019", "status": "Settled", "Amount": 125422, "settled_by": "Self" },
    { "name": "Aradhay", "mobile": 154541202, "ticket": 524587, "date": "12-05-2019", "status": "Requested", "Amount": 125412, "settled_by": " " },
    { "name": "Dilip", "mobile": 15251202, "ticket": 527857, "date": "12-05-2019", "status": "Requested", "Amount": 125487, "settled_by": " " },
    { "name": "Alok", "mobile": 63258755, "ticket": 524327, "date": "12-05-2019", "status": "Dispatched", "Amount": 125499, "settled_by": "Self" },
    { "name": "Sunil", "mobile": 154254122, "ticket": 524997, "date": "12-05-2019", "status": "Settled", "Amount": 125400, "settled_by": "Distributor" },
    { "name": "Sushil", "mobile": 6325858, "ticket": 526987, "date": "12-05-2019", "status": "Requested", "Amount": 12533, "settled_by": " " },
    { "name": "Sushma", "mobile": 9525858, "ticket": 5245587, "date": "12-05-2019", "status": "Dispatched", "Amount": 12533, "settled_by": "Distributor" },
    { "name": "Sheo", "mobile": 15541202, "ticket": 5245896, "date": "12-05-2019", "status": "Settled", "Amount": 12104, "settled_by": "Distributor" }
  ];


}
