import {Component, OnInit} from '@angular/core';
import * as $ from '../../../../../node_modules/jquery';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import {LotteryStorageService} from '../../../services/lottery-storage.service';
import { DatePipe } from '@angular/common';
import { tick } from '@angular/core/src/render3';
@Component({
  selector: 'app-result-view',
  templateUrl: './result-view.component.html',
  styleUrls: ['./result-view.component.scss']
})
export class ResultViewComponent implements OnInit {

  constructor(private lotteryService: LotteryHttpService, 
    private storageService: LotteryStorageService,private datePipe:DatePipe) {
  }
  today: number = Date.now();
  bookingslot: any=[];
  resultList: any = [];
  newList: any = [];
  tempList : any = [];
  num:String;
  newNum:string;
  oneday: any;
  isExist:Boolean;
  searchText: any;
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber;
  phoneDisp = "(" + this.phoneNumber + ")";
  role = this.userD.user.roleMaster.name;
  ngOnInit() {
  this.getSlotList();
 
  }

flag=0;

 getSlotList(){
     this.lotteryService.makeRequestApi('get', 'slotlist').subscribe(slot => {
       if (slot !=null) {
       this.bookingslot= slot;
       console.log('booking date', this.bookingslot);
       this.todayResult(this.bookingslot[0].id);
       }
       else{
         console.log('error');
       }

     });
   }
   yesterday(){   
     this.flag=1;
    $('#next').hide();
    $('#back').show();
    var dte = new Date();
    dte.setDate(dte.getDate() - 1);
this.oneday= dte.toString();
console.log(this.oneday);
this.lotteryService.makeRequestApi('post', 'viewResults', {bookingDate: this.datePipe.transform(dte,"dd-MM-yyyy"),slotMaster:{id:1 }}).subscribe(
  res => {
        this.resultList=res;
        console.log(this.resultList);
        this.tempList = res;
   });

   }
   todayResult(slotId:any){
     this.flag=0;
    $('#next').show();
    $('#back').hide();
      this.lotteryService.makeRequestApi('post', 'viewResults', {bookingDate: this.datePipe.transform(new Date(),"dd-MM-yyyy"),slotMaster:{id:1 }}).subscribe(
        res => {
              this.resultList=res;
              this.tempList = res;
              // console.log(this.resultList);
         });
       
   
   }

   getResult(slotId:any){

    if(this.flag==1){
      var dte = new Date();
      dte.setDate(dte.getDate() - 1);
  this.oneday= dte.toString();
      this.lotteryService.makeRequestApi('post', 'viewResults', {bookingDate: this.datePipe.transform(dte,"dd-MM-yyyy"),slotMaster:{id:slotId }}).subscribe(
        res => {
              this.resultList=res;
              this.tempList = res;
              // console.log(this.resultList);
         });
    
    }
    else{
      $('#next').show();
      $('#back').hide();
        this.lotteryService.makeRequestApi('post', 'viewResults', {bookingDate: this.datePipe.transform(new Date(),"dd-MM-yyyy"),slotMaster:{id:slotId }}).subscribe(
          res => {
                this.resultList=res;
                this.tempList = res;
                // console.log(this.resultList);
           });
    }
   
       
   
   }

   search1(){
    console.log(this.searchText);
    for(let item of this.tempList){
      if(this.searchText==item.gherNo){
           this.resultList= [];
           this.resultList.push(
            item
           )
          
           this.searchText=""; 
      }
      console.log(this.resultList);
  
    }
   }

  //  search(){
  
  //    console.log(this.searchText);
  //    if (this.searchText){
  //    this.newList = [];
  //    this.isExist = false;
  //    for(let item of this.tempList){

  //         this.num = "";
  //      console.log(item.gherNo);
  //      this.newNum = item.gherNo;
  //      console.log(this.newNum);

  //     //  console.log(item.luckyNumber);
  //      if (item.gherNo < 10){
  //         this.newNum = "0"+item.gherNo;
  //      }else {
  //        this.newNum +="" + item.gherNo;
  //      }
  //     //  if (item.luckyNumber < 10){
  //     //    this.num += "0"+item.luckyNumber
  //     //  }else {
  //     //    this.num += ""+ item.luckyNumber
  //     //  }
  //      console.log(this.newNum);
      
  //      if (this.newNum == this.searchText){
  //       this.resultList = [];
  //       this.resultList.push(item);
  //        console.log(this.newList);
  //        console.log('enters');
  //        this.isExist = true;
  //        break;
  //      }
       
  //    }
  //    if (this.isExist == false){
  //     this.resultList = [];
  //  }
  // }else {
  //   this.resultList = this.tempList;
  // }
  //  }
  }



