import { Component, OnInit, ViewChild } from '@angular/core';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { HttpClient } from "@angular/common/http";
import { LotteryHttpService } from '../../../services/lottery-http.service';
import * as $ from '../../../../../node_modules/jquery';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-wallet-details',
  templateUrl: './wallet-details.component.html',
  styleUrls: ['./wallet-details.component.scss']
})
export class WalletDetailsComponent implements OnInit {
  @ViewChild('walletModal') walletModal: any;
  options: any;
  userOtp: any = {
    pin1: '',
    pin2: '',
    pin3: '',
    pin4: ''
  };
  today: number = Date.now();
  amount;
  balance: any;
  debitBalance: any = [];
  creditBalance: any = [];
  allListTransaction: any = [];
  Balance: any = {
    "amount": "",
  }
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber;
  phoneDisp = "(" + this.phoneNumber + ")";
  role = this.userD.user.roleMaster.name;
  mobile: Number;
  ticket: Number;
  tempMobile: Number;
  tempTicket: Number;
  status: String;
  allTransaction: any = [];
  allDebit: any = [];
  allCredit: any = [];
  allBonusWallet: any = [];
  allBonusClaim: any = [];
  transactions: any = [];
  walletBonusBalance: any = [];
  claimBonusBalance: any = [];
  constructor(private router: Router, private sweetAlert: SweetAlertService, private storageService: LotteryStorageService,
     private http: HttpClient, private lotteryService: LotteryHttpService) { }
  clicked() {
    this.tempMobile = this.mobile;
    this.tempTicket = this.ticket;
  }
  navModel: any = {
    "Credit": 1,
    "Debit": 1,
    "ShowTransaction": 2,
    "ShowRequested": 1,
    "BonusClaim": 1,
    "BonusWallet": 1,
  }
  navbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel.Credit = 2;
      this.navModel.Debit = 1;
      this.navModel.ShowTransaction = 1;
      this.navModel.ShowRequested = 1;
      this.navModel.BonusClaim = 1;
      this.navModel.BonusWallet = 1;
    }
    else if (input == 2) {
      this.navModel.Credit = 1;
      this.navModel.Debit = 2;
      this.navModel.ShowTransaction = 1;
      this.navModel.ShowRequested = 1;
      this.navModel.BonusClaim = 1;
      this.navModel.BonusWallet = 1;
    }
    else if (input == 3) {
      this.navModel.Credit = 1;
      this.navModel.Debit = 1;
      this.navModel.ShowTransaction = 1;
      this.navModel.ShowRequested = 1;
      this.navModel.BonusClaim = 1;
      this.navModel.BonusWallet = 2;
    } else if (input == 4) {
      this.navModel.Credit = 1;
      this.navModel.Debit = 1;
      this.navModel.ShowTransaction = 1;
      this.navModel.ShowRequested = 1;
      this.navModel.BonusClaim = 2;
      this.navModel.BonusWallet = 1;
    }
    else if (input == 5) {
      this.navModel.Credit = 1;
      this.navModel.Debit = 1;
      this.navModel.ShowTransaction = 2;
      this.navModel.ShowRequested = 1;
      this.navModel.BonusClaim = 1;
      this.navModel.BonusWallet = 1;
    }
    else if (input == 6) {
      this.navModel.Credit = 1;
      this.navModel.Debit = 1;
      this.navModel.ShowTransaction = 1;
      this.navModel.ShowRequested = 2;
      this.navModel.BonusClaim = 1;
      this.navModel.BonusWallet = 1;
    }
  }

  ngOnInit() {
    $('.allTransactionForm').show();
    $('.creditForm').hide();
    $('.debitForm').hide();
    $('.bonusWalletForm').hide();
    $('.bonusClaimForm').hide();
    $('.showRequestedForm').hide()
    this.walletBalance();
    this.createUserModel();
  }
  //url:any = 'http://localhost:8080/'; http://env-0661550.mj.milesweb.cloud/wallet/
  url: any = 'http://env-0661550.mj.milesweb.cloud/wallet/';
  userInfo: any = {}
  balanceInfo: any = {}

  user: any = {};
  createUserModel() {
    this.userInfo = this.storageService.get('currentUser');
    this.user = {
      "userName": this.userInfo.user.userName,
      "email": this.userInfo.user.email,
      "userId": this.userInfo.user.id
    }
    this.getBalance();
    this.getPassbook();
    this.transactionList;
  }
  getBalance() {
    this.http.post(this.url + 'users/balance', this.user).subscribe(response => {
      this.balanceInfo = JSON.parse(JSON.stringify(response));
    });
  }
  getPassbook() {
    this.http.get(this.url + this.user.userId + '/passbook').subscribe(response => {
      this.transactionList = JSON.parse(JSON.stringify(response));
    });
  }
  getTransaction() {
    const reqMap = {
      id: this.storageService.get('currentUser').user.id,

    }
    this.lotteryService.makeRequestApi('post', 'getAllUserListByCreator', {
      id: this.storageService.get('currentUser').user.id,

    }).subscribe(res => {

      this.transactions = res;
      console.log('client check', this.transactions);
      this.allTransaction = this.transactions;
      this.allDebit = this.transactions;
      this.allCredit = this.transactions;
      this.allBonusClaim = this.transactions;
      this.allBonusWallet = this.transactions;
      console.log('all client check ', this.transactions);
    });
  }
  walletBalance() {

    let reqMap = {
      requestBy: {
        id: this.storageService.get('currentUser').user.id,
      }
    }
    this.lotteryService.makeRequestApi('post', 'viewWalletBalance', reqMap).subscribe((res) => {
      this.balance = res;
      console.log(this.balance.content.amount);
      this.Balance.amount = this.balance.content.amount;
    });

  }
  requestedList: any = [];

  balanceEntry: any = {
    "firstName": "",
    "status": "",
    "amount": "",
    "createdDate": "",
    "bankName": "",
    "id": ""
  }
  showRequest() {
    $('.allTransactionForm').hide();
    $('.creditForm').hide();
    $('.debitForm').hide();
    $('.bonusWalletForm').hide();
    $('.bonusClaimForm').hide();
    $('.showRequestedForm').show();
    let reqMap = {
      requestBy: {
        id: this.storageService.get('currentUser').user.id,
      }
    }
    this.lotteryService.makeRequestApi('post', 'viewWalletRequest', reqMap).subscribe((res) => {
      this.balance = res.content;


      for (var j = 0; j < this.balance.length; j++) {


        this.requestedList = this.balance[j]
        if (this.requestedList != null) {
          // this.balanceEntry.firstName = this.requestedList.requestTo.firstName;
          // this.balanceEntry.amount = this.requestedList.amount;
          // this.balanceEntry.createDate = this.requestedList.createDate;
          // this.balanceEntry.bankName = this.requestedList.bankName;
          // this.balanceEntry.status = this.requestedList.status.name;
          // this.balanceEntry.id = this.requestedList.id;
        }
      }
      console.log(this.requestedList.id);



    });

  }
  creditList: any = [];
  creditEntry: any = {
    "firstName": "",
    "transactionAmount": "",
    "transactionDate": "",
    "transactionMode": "",


  }
  showCredit() {
    $('.allTransactionForm').hide();
    $('.creditForm').show();
    $('.debitForm').hide();
    $('.bonusWalletForm').hide();
    $('.bonusClaimForm').hide();
    $('.showRequestedForm').hide();
    let reqMap = {
      user: {
        id: this.storageService.get('currentUser').user.id
      },
      transactionMode: "Credit"
    }
    this.lotteryService.makeRequestApi('post', 'viewCredit', reqMap).subscribe((res) => {
      this.creditBalance = res.content;


      for (var j = 0; j < this.creditBalance.length; j++) {
        this.creditList = this.creditBalance[j]
      }
      console.log(this.creditList);
      console.log(this.creditList.transactionAmount);
      console.log(this.creditList.transactionDate);
      console.log(this.creditList.transactionMode);
      console.log(this.creditList.user.firstName);


      // this.creditEntry.firstName = this.creditList.user.firstName;
      // this.creditEntry.transactionMode = this.creditList.transactionMode;
      // this.creditEntry.transactionDate = this.creditList.transactionDate;
      // this.creditEntry.transactionAmount = this.creditList.transactionAmount;


    });

  }
  debitList: any = [];
  debitEntry: any = {
    "firstName": "",
    "transactionAmount": "",
    "transactionDate": "",
    "transactionMode": "",
  }
  showDebit() {
    $('.allTransactionForm').hide();
    $('.creditForm').hide();
    $('.debitForm').show();
    $('.bonusWalletForm').hide();
    $('.bonusClaimForm').hide();
    $('.showRequestedForm').hide();
    let reqMap = {
      user: {
        id: this.storageService.get('currentUser').user.id
      },
      transactionMode: "Debit"
    }
    this.lotteryService.makeRequestApi('post', 'viewCredit', reqMap).subscribe((res) => {
      this.debitBalance = res.content;
      for (var j = 0; j < this.debitBalance.length; j++) {
        this.debitList = this.debitBalance[j]
        if (this.debitList != null) {
          // this.debitEntry.firstName = this.debitList.user.firstName;
          // this.debitEntry.transactionMode = this.debitList.transactionMode;
          // this.debitEntry.transactionDate = this.debitList.transactionDate;
          // this.debitEntry.transactionAmount = this.debitList.transactionAmount;
        }
      }
    });
  }
  wallletBonusList: any = [];
  wallletBonusEntry: any = {
    "firstName": "",
    "transactionAmount": "",
    "transactionDate": "",
    "transactionType": "",
  }
  walletBonus() {
    $('.allTransactionForm').hide();
    $('.creditForm').hide();
    $('.debitForm').hide();
    $('.bonusWalletForm').show();
    $('.bonusClaimForm').hide();
    $('.showRequestedForm').hide();
    let reqMap = {
      user: {
        id: this.storageService.get('currentUser').user.id
      },
      transactionType: "WalletBonus"
    }
    this.lotteryService.makeRequestApi('post', 'viewWalletBonus', reqMap).subscribe((res) => {
      this.walletBonusBalance = res.content;


      for (var j = 0; j < this.walletBonusBalance.length; j++) {

        this.wallletBonusList = this.walletBonusBalance[j]

        if (this.wallletBonusList != null) {
          console.log(this.wallletBonusList);
          // this.wallletBonusEntry.firstName = this.debitList.user.firstName;
          // this.wallletBonusEntry.transactionType = this.debitList.transactionType;
          // this.wallletBonusEntry.transactionDate = this.debitList.transactionDate;
          // this.wallletBonusEntry.transactionAmount = this.debitList.transactionAmount;
        }
      }
    });
  }

  claimBonusList: any = [];
  claimBonusEntry: any = {
    "firstName": "",
    "transactionAmount": "",
    "transactionDate": "",
    "transactionType": "",
  }
  claimBonus() {
    $('.allTransactionForm').hide();
    $('.creditForm').hide();
    $('.debitForm').hide();
    $('.bonusWalletForm').hide();
    $('.bonusClaimForm').show();
    $('.showRequestedForm').hide();
    let reqMap = {
      user: {
        id: this.storageService.get('currentUser').user.id
      },
      transactionType: "ClaimBonus"
    }
    this.lotteryService.makeRequestApi('post', 'viewWalletBonus', reqMap).subscribe((res) => {
      this.claimBonusBalance = res.content;
      for (var j = 0; j < this.claimBonusBalance.length; j++) {
        this.claimBonusList = this.claimBonusBalance[j]
        if (this.claimBonusList != null) {
          // this.claimBonusEntry.firstName = this.debitList.user.firstName;
          // this.claimBonusEntry.transactionType = this.debitList.transactionType;
          // this.claimBonusEntry.transactionDate = this.debitList.transactionDate;
          // this.claimBonusEntry.transactionAmount = this.debitList.transactionAmount;
        }
      }
    });
  }
  approv(emp) {
    let reqMap = {
      id: emp,
      requestBy: {
        id: this.storageService.get('currentUser').user.id
      }
    }
    console.log(reqMap);
    this.lotteryService.makeRequestApi('post', 'approv', reqMap).subscribe((res) => {

    });
    this.sweetAlert.swalSuccess('Claim Approved');
    this.showRequest();
  }

  addMoney() {
    let reqMap = {

      requestBy: {
        id: this.storageService.get('currentUser').user.id
      },
      requestTo: {
        id: this.storageService.get('currentUser').user.createdBy
      },
      paymentMode: "cash",
      bankName: "united",
      remarks: "remarkkk",
      chequeNo: "2383388383",
      bankTransactionId: "83kdi3ikdkd",
      name: "ranjeet",
      amount: this.amount,
      phoneNo: 438383838
    }
    console.log(reqMap);
    this.lotteryService.makeRequestApi('post', 'requestWalletAmount', reqMap).subscribe((res) => {

    });
    this.sweetAlert.swalSuccess('Amount Requested');
    this.router.navigate(['/client/wallet-Details']);
    this.walletModal.hide();
  }
  allTransactionDone() {
    $('.allTransactionForm').show();
    $('.creditForm').hide();
    $('.debitForm').hide();
    $('.bonusWalletForm').hide();
    $('.bonusClaimForm').hide();
    $('.showRequestedForm').hide();
    let reqMap = {

      user: {
        id: this.storageService.get('currentUser').user.id
      }
    }
    console.log(reqMap);
    this.lotteryService.makeRequestApi('post', 'allTransactionList', reqMap).subscribe((res) => {
      this.allListTransaction = res;
    });

    // this.sweetAlert.swalSuccess('Amount Requested');
    // this.router.navigate(['/client/wallet-Details']);
    // this.walletModal.hide();
  }
  transactionList: any = [
    {
      "id": 8,
      "userAccountId": 1,
      "userName": null,
      "amount": 100.00,
      "details": "details",
      "transactionDate": "2019-12-02",
      "transactionReference": 12
    },
    {
      "id": 9,
      "userAccountId": 1,
      "userName": null,
      "amount": 500.00,
      "details": "details",
      "transactionDate": "2019-12-02",
      "transactionReference": 12
    },
    {
      "id": 22,
      "userAccountId": 1,
      "userName": null,
      "amount": -100.00,
      "details": "details",
      "transactionDate": "2019-12-02",
      "transactionReference": 12
    },
    {
      "id": 24,
      "userAccountId": 1,
      "userName": null,
      "amount": -100.00,
      "details": "details",
      "transactionDate": "2019-12-02",
      "transactionReference": 12
    },
    {
      "id": 26,
      "userAccountId": 1,
      "userName": null,
      "amount": -100.00,
      "details": "details",
      "transactionDate": "2019-12-02",
      "transactionReference": 12
    },
    {
      "id": 28,
      "userAccountId": 1,
      "userName": null,
      "amount": -100.00,
      "details": "details",
      "transactionDate": "2019-12-02",
      "transactionReference": 12
    },
    {
      "id": 30,
      "userAccountId": 1,
      "userName": null,
      "amount": -100.00,
      "details": "details",
      "transactionDate": "2019-12-02",
      "transactionReference": 12
    },
    {
      "id": 39,
      "userAccountId": 1,
      "userName": null,
      "amount": -100.00,
      "details": "details",
      "transactionDate": "2019-12-02",
      "transactionReference": 12
    },
    {
      "id": 71,
      "userAccountId": 1,
      "userName": "user1",
      "amount": 300.00,
      "details": "testing Money",
      "transactionDate": null,
      "transactionReference": 1234567890
    }
  ]
}
