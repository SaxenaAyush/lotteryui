import {Component} from '@angular/core';


@Component({
  selector: 'app-wallet-modal',
  template: `
    <div (click)="onContainerClicked($event)" class="modal fade" tabindex="-1" [ngClass]="{'in': visibleAnimate}"
         [ngStyle]="{'display': visible ? 'block' : 'none', 'opacity': visibleAnimate ? 1 : 0}">
      <div class="vertical-alignmenthelper">
        <div class="vertical-aligncenter">
          <div class="">
            <div class="{{width}}">
              <div class="modal-content">

                <div class="">
                  <ng-content select=".app-wallet-modal-body"></ng-content>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .modal {
      background: rgba(0, 0, 0, 0.8);
      width: 100%;
      height: 100%;
      max-height: 100%;
      z-index: 999;
      align-self: center;

    }

    .modal-body {
      max-height: calc(100vh - 210px);
      overflow-y: auto;
    }

    .vertical-alignmenthelper {
      display: table;
      height: 100%;
      width: 50%;
      margin-left: 306px ;
      pointer-events: none;
    }

    .vertical-aligncenter {
      /* To center vertically */
      display: table-cell;
      vertical-align: middle;
      pointer-events: none;
    }

    .modal-content {
      /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
      width: inherit;
      max-width: inherit; /* For Bootstrap 4 - to avoid the modal window stretching full width */
      height: inherit;
      /* To center horizontally */
      margin: 0 auto;
      pointer-events: all;
      background-color: #ffffff;
    }

    .modal-footer {
      background-color: transparent;
    }

    .modal-header {
      font-weight: 600;
      font-size: 18px;
    }

    .btn {
      margin-right: 10px;
    }
  `]
})
export class WalletModalComponent {

  public visible = false;
  public visibleAnimate = false;
  public width = '';

  constructor() {
  }

  public show(): void {
    this.visible = true;
    this.width = 'col-sm-12';

    setTimeout(() => this.visibleAnimate = true, 100);
  }

  public showSmall(): void {
    this.visible = true;
    this.width = 'container';
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  public setDataAndShow(title: String, body: string): void {
    let header = document.getElementsByClassName("modal-header")[0];
    header.innerHTML = '<h6>' + title + '</h6>';
    let body1 = document.getElementsByClassName("modal-body")[0];
    body1.innerHTML = body;
    this.visible = true;
    this.width = 'col-sm-8 col-sm-offset-2';
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  public hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);
  }

  public onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains('modal')) {
      //this.hide();
    }
  }

}
