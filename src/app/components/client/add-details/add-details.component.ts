import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import * as $ from '../../../../../node_modules/jquery';
import { FileUploader } from 'ng2-file-upload';
import { environment } from "../../../../environments/environment";

@Component({
  selector: 'app-add-details',
  templateUrl: './add-details.component.html',
  styleUrls: ['./add-details.component.scss']
})
export class AddDetailsComponent implements OnInit {
  public clientDetailsForm: FormGroup
  public bankDetailsForm: FormGroup
  public kycDetailsForm: FormGroup
  public uploader: FileUploader = new FileUploader({});
  imageUrl: string | ArrayBuffer;
  imagePK: any;
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber;
  phoneDisp = "(" + this.phoneNumber + ")";
  role = this.userD.user.roleMaster.name;
  isEdit = false;
  loggedInUserDetails = this.storageService.get('currentUser');
  userid = this.loggedInUserDetails.user.id;
  constructor(private storageService: LotteryStorageService, private alertService: SweetAlertService,
    private router: Router, public route: ActivatedRoute, private fb: FormBuilder,
    private lotteryService: LotteryHttpService) {
  }


  ngOnInit() {


    console.log(this.loggedInUserDetails);

    this.clientDetailsForm = this.fb.group({
      shopName: ['', [Validators.required]],
      address: ['', [Validators.required]],
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      pin: ['', Validators.required],
      addressType: ['', Validators.required],
      status: [0],
    });
    this.bankDetailsForm = this.fb.group({
      bankName: ['', [Validators.required]],
      accountNumber: ['', [Validators.required]],
      ifscCode: ['', [Validators.required]],
      branchName: ['', [Validators.required]],
      status: [0],

    });
    this.kycDetailsForm = this.fb.group({
      docName: ['', Validators.required],
      docNumber: ['', Validators.required],
      status: [1],

    });
  }

  addClientDetails() {
    $('.kyc-form').hide();
    $('.bank-form').show();
    $('.address-form').hide();

    let reqMap = {
      ...this.clientDetailsForm.value,
      user: {
        id: this.userid
      },
      status: {
        id: 1
      }
    };
    console.log(reqMap);
    this.lotteryService.makeRequestApi('post', 'personalDetail', reqMap).subscribe((res) => {
      this.alertService.swalSuccess('Client Successfully Added');
    }, err => {
      console.log(err);
    });

  }


  addBankDetails() {
    $('.kyc-form').show();
    $('.bank-form').hide();
    $('.address-form').hide();
    let reqMap = {
      bankDetails: this.bankDetailsForm.value,
      user: {
        id: this.userid
      },
      status: {
        id: 1
      }
    };
    console.log(reqMap);
    this.lotteryService.makeRequestApi('post', 'bankDetails', reqMap).subscribe((res) => {
      console.log(res);
      this.alertService.swalSuccess('Bank Details Successfully Added');
    }, err => {
      console.log(err);
    });

  }

  addKycDetails() {

    let reqMap = {
      kycDetails: this.kycDetailsForm.value,
      user: {
        id: this.userid
      },
      status: {
        id: 1
      }
    };
    console.log(reqMap);
    this.lotteryService.makeRequestApi('post', 'kycDetails', reqMap).subscribe((res) => {
      console.log(res);
      this.alertService.swalSuccess('KYC Successfully Added');
      this.router.navigate(['/client/user-profile']);
    }, err => {
      console.log(err);
    });

  }

}
