import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { LotteryStorageService } from '../../services/lottery-storage.service';
import { HttpClient } from '@angular/common/http';
import { LotteryHttpService } from '../../services/lottery-http.service';
import { SweetAlertService } from '../../common/sharaed/sweetalert2.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FormGroup, FormBuilder } from '@angular/forms';


@Component({
selector: 'app-collapsing',
templateUrl: './collapsing.component.html',
styleUrls: ['./collapsing.component.scss']
})
export class CollapsingComponent implements OnInit {
  loggedInUserDetails: any;
  allClients:any=[];
  viewClientDetails: any =[];
  Client:any=[];
  allInActiveClients: any=[];
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  phoneNumber =  this.userD.user.phoneNumber;
phoneDisp = "("+ this.phoneNumber +")";
  role = this.userD.user.roleMaster.name;
  public walletForm: FormGroup;
public licenseForm: FormGroup;
today: number = Date.now();

id= this.userD.user.roleMaster.id;
  constructor( private http: HttpClient, private fb: FormBuilder, private storageService: LotteryStorageService, private router: Router, private alertService: SweetAlertService, private lotteryService: LotteryHttpService) {
  }


  ngOnInit() {
    $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
    // console.log("ip");
    // this.userService.getIpAddress().subscribe(data => {
    //   console.log(data);
    // });
  this.getEmployee();
  this.initLicenseForm();

 }
initWalletForm(){
  this.walletForm = this.fb.group({
    phoneNumber: [''],
    comment: [''],
    name: [''],
    transactionNo: [''],
    chequeNo: ['']
  });
}

initLicenseForm(){
  this.licenseForm= this.fb.group({
    fromDate: [''],
    toDate: [''],
  })
}
  openAddClient() {
    this.router.navigate(['/distributor/add-new-client']);
  }

  openActivatedClient() {
    $('.activated-Form').show();
    $('.deactivated-Form').hide();

  }

  openDeactivatedClient() {
    $('.activated-Form').hide();
    $('.deactivated-Form').show();
  }

  deleteClient(emp) {
    console.log('emp', emp)
    Swal.fire({
      html: '<h3 class="mt-5 mb-4 mx-2">Are you sure you want to deactivate this client?</h3>',
      animation: true,
      width: 548,
      padding: '-1.9em',
      showCancelButton: true,
      focusConfirm: false,
      cancelButtonText: 'No',
      cancelButtonColor: '#17A2B8',
      cancelButtonClass: 'btn btn-outline-info btn-pill px-4 mr-2',
      confirmButtonColor: '#DD6B55',
      confirmButtonClass: 'btn btn-danger btn-pill px-4',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.lotteryService.makeRequestApi('post', 'deleteClient', {
        
          id: emp,
          status: 2
        }).subscribe(res => {
          if (!res.isError) {
            this.getEmployee();
          }
        });
      }
    });

  }

  activeClient(emp) {
    console.log('emp', emp)
    Swal.fire({
      html: '<h3 class="mt-5 mb-4 mx-2">Are you sure you want to Activate this client?</h3>',
      animation: true,
      width: 548,
      padding: '-1.9em',
      showCancelButton: true,
      focusConfirm: false,
      cancelButtonText: 'No',
      cancelButtonColor: '#17A2B8',
      cancelButtonClass: 'btn btn-outline-info btn-pill px-4 mr-2',
      confirmButtonColor: '#DD6B55',
      confirmButtonClass: 'btn btn-danger btn-pill px-4',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.lotteryService.makeRequestApi('post', 'deleteClient', {
        
          id: emp,
          status: 1
        }).subscribe(res => {
          if (!res.isError) {
            this.getEmployee();
          }
        });
      }
    });

  }
  getEmployee() {
    const reqMap = {
      id: this.storageService.get('currentUser').user.id,

    }
    this.lotteryService.makeRequestApi('post', 'getAllUserListByCreator', {
           id: this.storageService.get('currentUser').user.id,
    
           }).subscribe(res => {
       
      this.Client = res;
      console.log('client check', this.Client);
      this.allClients= this.Client.content.Active;
      this.allInActiveClients= this.Client.content.InActive;
      console.log('all client check ',this.allClients);
      });
      
  }
  viewClient(emp){
    this.lotteryService.makeRequestApi('post', 'viewClient', {        
      id: emp,
      status: 1
    }).subscribe(res => {
      console.log('hello user data',res);
      this.storageService.set('viewUser', res);      
      this.viewClientDetails = res;
      this.router.navigate(['/distributor/view-client-details']);
    });
  }
openCash(){
  $('.cash').show();
  $('.neft').hide();
  $('.cheque').hide();
 
}
opencheque(){
  $('.cash').hide();
  $('.neft').hide();
  $('.cheque').show();
 
}
openNeft(){
  $('.cash').hide();
  $('.neft').show();
  $('.cheque').hide();

}
userId(emp){
  this.id= emp
  console.log(this.id);
  
}
licenseRenewal(){
  let reqMap = {
        ...this.licenseForm.value,
        loginId: this.storageService.get('currentUser').user.id,
        userId: this.id,
      };
      this.id= 0;
      console.log(reqMap);
//       this.lotteryService.makeRequestApi('post', 'licenseRenewal',reqMap).subscribe(res => {
//         console.log('hello user data',res);
//       
//         this.router.navigate(['/distributor/view-client-details']);
//       });
 }

 machineReset(){
  let reqMap = {
       
        loginId: this.storageService.get('currentUser').user.id,
        userId: this.id,
      };
      this.id= 0;
      console.log(reqMap);
//       this.lotteryService.makeRequestApi('post', 'resetMachine',reqMap).subscribe(res => {
//         console.log('hello user data',res);
//        
//         this.router.navigate(['/distributor/view-client-details']);
//       });
 }
// sendMoney(){
//   let reqMap = {
//     ...this.signupForm.value
//   };
//   delete reqMap.newPassword;
//   this.curryLeavesHttpService.makeRequestApi('post', 'signUpGuestUser', reqMap).subscribe((res) => {
//     i
//     } else {
//       this.alertService.swalError(res.message);
//     }
//   }, err => {
//     console.log(err);
//   });
// }
}


