import {Component, OnInit} from '@angular/core';
import * as $ from '../../../../node_modules/jquery';
import {ActivatedRoute, Router} from '@angular/router';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {SweetAlertService} from '../../common/sharaed/sweetalert2.service';
import Swal from 'sweetalert2';
import {LotteryStorageService} from '../../services/lottery-storage.service';
import {LotteryHttpService} from '../../services/lottery-http.service';
import {version} from '../../../../package.json';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  loading: boolean;
id:any
  constructor(private storageService: LotteryStorageService,
              private lotteryHttpService: LotteryHttpService, private fb: FormBuilder,
              private router: Router, private route: ActivatedRoute, private alertService: SweetAlertService) {
  }

appVersion:any =' '+ version;

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required]],
      passwordHash: ['', [Validators.required]],
    });
  }

  userModel : any = {
    "email" : "",
  }

  loginUser() {
    if (this.loginForm.valid) {
      this.loading = false;
      const reqMap = {
        passwordHash: this.loginForm.value.passwordHash,
        licence:{
          //  machinekey: "qwss234ecf"
            machinekey:"dkeie76jdk"
        }
        };
      if (this.loginForm.value.email.match(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)) {
        reqMap['email'] = this.loginForm.value.email;
      } else {
        reqMap['phoneNumber'] = this.loginForm.value.email;
      }

      this.lotteryHttpService.makeRequestApi('post', 'userLogin', reqMap).subscribe(res => {
        if (res.code == 206) {
          const respCopy = (res);
          if (res.user.roleMaster.id === 1  || res.user.roleMaster.id ===3 || res.user.roleMaster.id === 2 ) {
            this.router.navigate(['/client/wallet-Details']);
            console.log('check details',res);
            console.log(res.userName);
          } else if (res.user.roleMaster.id === 4) {
            this.router.navigate(['/client/booking']);
            console.log('check details',res);
            console.log(res.userName);
this.id = respCopy.user.id;
          }
          this.storageService.set('currentUser', respCopy);
          this.loading = false;

          // this.price();

        }        else {
          this.alertService.swalError(res.message);

        }
      });

     

    } else {
      this.alertService.swalError('Error In Data');
    }
  }


  resetUser() {
   console.log(this.userModel.email);
   if(this.userModel.email != ""){
    this.lotteryHttpService.makeRequestApi('post', 'reset', ({
      email: this.userModel.email
    })).subscribe(res => {
  
      if (res.code == 207) { 
        const respCopy = (res);
        console.log(res);
        console.log(respCopy);
     this.userModel.email= "";
     this.alertService.swalError('New Password is saved ');
      }  else if(res.code==209){
        this.alertService.swalError('email id is wrong ');
        this.userModel.email= "";
      }  
    });
   }
     
  }
resetemail :any;

//   resetUser() {
//     if (this.loginForm.valid) {
//       this.loading = false;
//       const reqMap = {
//         passwordHash: this.loginForm.value.passwordHash
//       };
//       if (this.loginForm.value.email.match(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)) {
//         reqMap['email'] = this.loginForm.value.email;
//       } else {
//         reqMap['phoneNumber'] = this.loginForm.value.email;
//       }

//       // this.lotteryHttpService.makeRequestApi('post', 'userLogin', reqMap).subscribe(res => {
//       //   if (res.code == 206) {
//       //     const respCopy = (res);
//       //     if (res.user.roleMaster.id === 1  || res.user.roleMaster.id ===3 || res.user.roleMaster.id === 2 ) {
//       //       this.router.navigate(['/distributor/view-client']);
//       //       console.log('check details',res);
//       //       console.log(res.userName);
//       //     } else if (res.user.roleMaster.id === 4) {
//       //       this.router.navigate(['/client/booking']);
//       //       console.log('check details',res);
//       //       console.log(res.userName);

//       //     }
//       //     this.storageService.set('currentUser', respCopy);
//       //     this.loading = false;

//       //     // this.price();

//       //   }        else {
//       //     this.alertService.swalError(res.message);

//       //   }
//       // });
// console.log('forget email',reqMap);
     

//     } else {
//       this.alertService.swalError('Error In Data');
//     }
//   }

    openForgotPassword() {
      $('.login-form').hide();
      $('.forget-form').show();

    }

  openLoginForm() {
    $('.forget-form').hide();
    $('.login-form').show();
  }
}
