import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ModalComponent } from './modal.component';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ElectronService } from './providers/electron.service';
import { TallyConnectorService } from './providers/tallyconnector.service';
import { WebviewDirective } from './directives/webview.directive';
import { AppComponent } from './app.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { BookingComponent } from './components/client/booking/booking.component';
import { ViewClientComponent } from './components/distributor/view-client/view-client.component';
import { DashboardComponent } from './components/distributor/dashboard/dashboard.component';
import { HomeLayoutComponent } from './components/layout/home-layout/home-layout.component';
import { LoginLayoutComponent } from './components/layout/login-layout/login-layout.component';
import { LoginComponent } from './components/login/login.component';
import { WalletDetailsComponent } from './components/client/wallet-details/wallet-details.component';
import { ClaimTransactionComponent } from './components/client/claim-transaction/claim-transaction.component';
import { WalletModalComponent } from './components/client/wallet-details/wallet-modal.component';
import { SharedModule } from './common/sharaed/sharaed.module';
import { HttpModule } from '@angular/http';
import { AddDetailsComponent } from './components/client/add-details/add-details.component';
import { ViewClientDetailsComponent } from './components/distributor/view-client-details/view-client-details.component';
import { AddNewClientComponent } from './components/distributor/add-new-client/add-new-client.component';
import { NgxSelectModule } from 'ngx-select-ex';
import { ResultViewComponent } from './components/client/result-view/result-view.component';
import { UserProfileComponent } from './components/client/user-profile/user-profile.component';
import { SettingComponent } from './components/client/setting/setting.component';
import { AdminProfileComponent } from './components/distributor/admin-profile/admin-profile.component';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { QRCodeModule } from 'angularx-qrcode';
import { DatePipe } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { ResultReportComponent } from './components/client/result-report/result-report.component';
import { MatIconModule } from '@angular/material';
import { CollapsingComponent } from './components/collapsing/collapsing.component';
  


const { remote } = require('electron');

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
// function countdownConfigFactory(): CountdownGlobalConfig {
//   return { format: `mm:ss` };
// }


@NgModule({
  declarations: [
    AppComponent,
   
    WebviewDirective,
    ModalComponent,
    WalletModalComponent,
    SideNavComponent,
    BookingComponent,
    ViewClientComponent,
    DashboardComponent,
    HomeLayoutComponent,
    LoginLayoutComponent,
    LoginComponent,
    WalletDetailsComponent,
    ClaimTransactionComponent,
    AddDetailsComponent,
    ViewClientDetailsComponent,
    AddNewClientComponent,
    ResultViewComponent,
    ResultReportComponent,
    UserProfileComponent,
    SettingComponent,
    AdminProfileComponent,
    HeaderComponent,
  CollapsingComponent

  ],
  imports: [
    SharedModule.forRoot(),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    HttpModule,
    NgxSelectModule,
    NgxQRCodeModule,
    QRCodeModule,

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],


  providers: [ElectronService, TallyConnectorService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule {
}
