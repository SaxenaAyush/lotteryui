import { Injectable } from '@angular/core';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import { ipcRenderer, webFrame, remote } from 'electron';
import * as childProcess from 'child_process';
import * as fs from 'fs';

@Injectable()
export class ElectronService {

  ipcRenderer: typeof ipcRenderer;
  webFrame: typeof webFrame;
  remote: typeof remote;
  childProcess: typeof childProcess;
  fs: typeof fs;
  VLC_PATH='C:\\\\Program Files\\VideoLAN\\VLC\\vlc.exe';
  constructor() {
    // Conditional imports
    if (this.isElectron()) {
      this.ipcRenderer = window.require('electron').ipcRenderer;
      this.webFrame = window.require('electron').webFrame;
      this.remote = window.require('electron').remote;

      this.childProcess = window.require('child_process');
      this.fs = window.require('fs');
    }
  }

  isElectron = () => {
    return window && window.process && window.process.type;
  }
   openVideo(serverPath){
    //serverPath = 'https://protactinium.gorillavid.in:8182/joorlxyiscu4tqukwzb3bhjqj4ypszwjtlprk2a4hiphwlke7frwi25vtu/c5e3ff6gyfr8.mp4';
    this.childProcess.exec('"'+this.VLC_PATH+'" '+serverPath );
    // cp.execFile(cmd, ['--version'], function (err, stdout) {
    //   if (err) return console.error(err)
    //   console.log(stdout)
    // })
  }
  openInBrowser(serverPath){
    this.childProcess.exec('explorer '+serverPath );
  }
  openInWindow(url){
    window.open(url, '_blank', 'nodeIntegration=no')
  }

}
