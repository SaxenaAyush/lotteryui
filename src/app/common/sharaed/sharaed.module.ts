import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SweetAlertService} from './../../common/sharaed/sweetalert2.service';


@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [SweetAlertService],
  exports: [
    ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [SweetAlertService]
    };
  }
}
