import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeLayoutComponent } from './components/layout/home-layout/home-layout.component';
import { DashboardComponent } from './components/distributor/dashboard/dashboard.component';
import { LoginLayoutComponent } from './components/layout/login-layout/login-layout.component';
import { LoginComponent } from './components/login/login.component';
import { BookingComponent } from './components/client/booking/booking.component';
import { ViewClientComponent } from './components/distributor/view-client/view-client.component';
import { WalletDetailsComponent } from './components/client/wallet-details/wallet-details.component';
import { ClaimTransactionComponent } from './components/client/claim-transaction/claim-transaction.component';
import { AddDetailsComponent } from './components/client/add-details/add-details.component';
import { ViewClientDetailsComponent } from './components/distributor/view-client-details/view-client-details.component';
import { AddNewClientComponent } from './components/distributor/add-new-client/add-new-client.component';
import { AuthGuard } from './components/guard/auth.guard';
import { ResultViewComponent } from './components/client/result-view/result-view.component';
import { UserProfileComponent } from './components/client/user-profile/user-profile.component';
import { SettingComponent } from './components/client/setting/setting.component';
import { AdminProfileComponent } from './components/distributor/admin-profile/admin-profile.component';
import { ResultReportComponent } from './components/client/result-report/result-report.component';
import { CollapsingComponent } from './components/collapsing/collapsing.component';
const routes: Routes = [

  {
    path: 'distributor',
    component: HomeLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'add-new-client',
        component: AddNewClientComponent
      },

      {
        path: 'view-client',
        component: ViewClientComponent
      },
      {
        path: 'view-client-details',
        component: ViewClientDetailsComponent
      },

      {
        path: 'admin-profile',
        component: AdminProfileComponent
      }
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent
      },


    ]
  },
  {
    path: 'client',
    component: HomeLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'booking',
        component: BookingComponent
      },
      {
        path: 'wallet-Details',
        component: WalletDetailsComponent
      },
      {
        path: 'claim-transaction',
        component: ClaimTransactionComponent
      },
      {
        path: 'result-view',
        component: ResultViewComponent
      },
      {
        path: 'add-details',
        component: AddDetailsComponent
      },
      {
        path: 'user-profile',
        component: UserProfileComponent
      },
      {
        path: 'setting',
        component: SettingComponent
      },
      {
        path: 'result-report',
        component: ResultReportComponent
      },

    ]
  },
{
path: 'collapsing',
component: CollapsingComponent
},
  { path: '**', redirectTo: '', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
