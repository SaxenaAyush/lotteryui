import { environment } from '../../environments/environment';

export class ConstantsHelper {


  public static readonly getAPIUrl = {

    addClient: () => `${environment.baseUrl}/lottery/signup`,
    userLogin: () => `${environment.baseUrl}/lottery/alldetailsignin`,
    booking: () => `${environment.baseUrl}/lottery/book-ticket`,
    kycDetails: () => `${environment.baseUrl}/lottery/add-kyc-detail`,
    personalDetail: () => `${environment.baseUrl}/lottery/add-personal-detail`,
    bankDetails: () => `${environment.baseUrl}/lottery/add-bank-detail`,
    getRoleList: () => `${environment.baseUrl}/lottery/get-roles`,
    getPassbookDetails: () => `${environment.baseUrl}/wallet/users/1/passbook`,
    slotlist: () => `${environment.baseUrl}/lottery/get-slot-list`,
    priceList: () => `${environment.baseUrl}/lottery/get-ticket-price`,
    //  userDetails: (id: number) => `${environment.baseUrl}/lottery/get-employeeById/`+id,
    //  getResults: (bookingDate= "25-05-2019",id= 1) => `${environment.baseUrl}/lottery/get-employeeById/`+bookingDate+'/' +id,
    viewResults: () => `${environment.baseUrl}/lottery/get-result`,
    clientDetails: () => `${environment.baseUrl}/lottery/get-employee`,
    getUserByCreator: () => `${environment.baseUrl}/lottery/getUserListByCreator`,
    getAllUserListByCreator: () => `${environment.baseUrl}/lottery/getAllUserListByCreator`,
    deleteClient: () => `${environment.baseUrl}/lottery/getChangeStatusOfUser`,
    reset: () => `${environment.baseUrl}/lottery/resetPassword`,
    changePassword: () => `${environment.baseUrl}/lottery/ChangePassword`,
    viewClient: () => `${environment.baseUrl}/lottery/userDetailSignin`,
    licenseRenewal: () => `${environment.baseUrl}/lottery/licence-renewal`,
    resetMachine: () => `${environment.baseUrl}/lottery/reset-machinekey`,
    settledCliam: () => `${environment.baseUrl}/lottery/settled-claim`,
    searchByNumber: () => `${environment.baseUrl}/lottery/find-my-number`,
    viewRequestClaim: () => `${environment.baseUrl}/lottery/view-requested-claim`,
    viewDispatchClaim: () => `${environment.baseUrl}/lottery/view-dispatch-claim`,
    viewAllTransactionClaim: () => `${environment.baseUrl}/lottery/myall-requested-claim`,
    viewWalletBalance: () => `${environment.baseUrl}/lottery/view-wallet-balance`,
    viewWalletRequest: () => `${environment.baseUrl}/lottery/view-wallet-request`,
    // viewDebit: () => `${environment.baseUrl}/lottery/view-debit`,
    viewCredit: () => `${environment.baseUrl}/lottery/view-debit-credit`,
    viewWalletBonus: () => `${environment.baseUrl}/lottery/view-wallet-claim-bonus`,
     approv: () => `${environment.baseUrl}/lottery/approve-wallet-request`,
     requestWalletAmount: () => `${environment.baseUrl}/lottery/request-wallet-amount`,
     allTransactionList: () => `${environment.baseUrl}/lottery/view-wallet-transaction`,
     getAmount: () => `${environment.baseUrl}/lottery/view-wallet-balance`,
     getAllResult: () => `${environment.baseUrl}/lottery/get-result-bydate`,
     commission: () => `${environment.baseUrl}/lottery/view-wallet-commission`,
     saveCommission: () => `${environment.baseUrl}/lottery/save-single-commission`,
     //  userDetails: (id: number) => `${environment.baseUrl}/lottery/get-employeeById/`+id,
     //  getResults: (bookingDate= "25-05-2019",id= 1) => `${environment.baseUrl}/lottery/get-employeeById/`+bookingDate+'/' +id,
    
    http://localhost:8080/find-my-number

    {
      "id": 160
    }
  };
  
}
